package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class LocationsPage {

    WebDriver driver;

    By city = By.xpath("//span[@class='title'][contains(text(),'Locations')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By address = By.id("AddressLine1");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath("(//a[@class='btn green-meadow btn-outline'])[2]");
    By delete = By.xpath("(//a[@class='btn red btn-outline'])[2]");
    By address2 = By.id("AddressLine2");


    public LocationsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickLocations() {

        driver.findElement(city).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void addCountry() {

        WebElement country = driver.findElement(By.xpath(".//*[@id='CityCountryID']"));
        Select country_dd = new Select(country);
        country_dd.selectByVisibleText("Bulgaria");
    }

    public void addCity() {

        WebElement city = driver.findElement(By.xpath(".//*[@id='ddlCities']"));
        Select city_dd = new Select(city);
        city_dd.selectByIndex(2);
    }

    public void typeAddress(String country) {

        driver.findElement(address).sendKeys(country);
    }

    public void typeAddress2(String country) {

        driver.findElement(address2).sendKeys(country);
    }

    public void editLocation() {

        driver.findElement(address).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A test location edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}