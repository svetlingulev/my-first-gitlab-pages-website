package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorklogsPage {

    WebDriver driver;

    By Save = By.xpath("//input[@class='btn green btn-outline']");
    By Worklogs = By.xpath("//span[@class='title'][contains(text(),'Worklogs')]");
    By LogWork = By.xpath("//span[@class='title'][contains(text(),'Log Work')]");
    By LogTime = By.xpath(".//*[@id='LoggedTime']");
    By BackToList = By.xpath("//span[text()='Back to list']");
    By ApprovedWorklogs = By.xpath("//span[@class='caption-subject'][contains(text(),'Approved Worklogs')]");
    By ListWorklogs = By.xpath("//span[@class='title'][contains(text(),'List Worklogs')]");
    By Search = By.xpath("//span[text()='Search']");
    By MyWorkLogs = By.xpath("//span[@class='title'][contains(text(),'My Work Logs')]");
    By Edit = By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[6]/a[1]/span");
    By Admin = By.xpath("//i[@class='fa fa-angle-down']");
    By Logout = By.xpath("html/body/div[2]/div/div[2]/ul/li/ul/li[2]/a");
    By ManageOvertime = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[12]/ul/li[3]/a");
    By Approve = By.xpath("(//span[text()='Approve'])[1]");
    By Reject = By.xpath("(//span[text()='Reject'])[1]");
    By rejectConfirm = By.xpath(".//*[@id='reject-confirm']");


    public WorklogsPage(WebDriver driver) {
        this.driver = driver;
    }



    public void clickWorkLogs() {

        driver.findElement(Worklogs).click();
    }

    public void clickLogWork() {

        driver.findElement(LogWork).click();
    }

    public void clickTypeButton() {

        WebElement radioBtn = driver.findElement(By.id("overtime"));

        radioBtn.click();
    }

    public void clickTypeButton2() {

        WebElement radioBtn = driver.findElement(By.id("normal"));

        radioBtn.click();
    }

    public void editProject() {

        WebElement Project = driver.findElement(By.id("ProjectID"));
        Select project_dd = new Select(Project);
        project_dd.selectByVisibleText("SevOne NMS");
        Assert.assertEquals(project_dd.getFirstSelectedOption().getText(),"SevOne NMS");
    }

    public void startDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerStart']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

    }

    public void endDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerEnd']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).sendKeys(Keys.ENTER);

    }

    public void startDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerStart']/span/button")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }

        }

    }

    public void endDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerEnd']/span/button")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }

        }
    }

    public void startDate3() {
        driver.findElement(By.xpath(".//*[@id='datepickerStart']/span/button")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("20")){
                dates.get(i).click();
                break;
            }

        }

    }

    public void endDate3() {
        driver.findElement(By.xpath(".//*[@id='datepickerEnd']/span/button")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("20")){
                dates.get(i).click();
                break;
            }

        }
    }

    public void sDate() {
        driver.findElement(By.xpath(".//*[@id='StartDate']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//td[@class='active day']")).click();

    }

    public void eDate() {
        driver.findElement(By.xpath(".//*[@id='EndDate']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//td[@class='active day']")).click();

    }



    public void typeLoggedTime() {

        driver.findElement(LogTime).sendKeys(Keys.chord(Keys.CONTROL, "a"), "8");

    }

    public void typeNewLoggedTime() {

        driver.findElement(LogTime).sendKeys(Keys.chord(Keys.CONTROL, "a"), "6");

    }

    public void clickSave() {

        driver.findElement(Save).click();
    }

    public void clickBackToList() {

        driver.findElement(BackToList).click();

    }

    public void clickApprovedWorklogs() {

        driver.findElement(ApprovedWorklogs).click();

    }

    public void clickOnDeleteButton() throws InterruptedException {

        WebElement element = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[6]/a[2]/span"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.xpath("//a[@class='btn btn-danger']/i[@class='fa fa-trash-o']"));
        if (element2.isDisplayed()) {
            element2.click();
        }
    }

    public void clickListWorklogs() {

        driver.findElement(ListWorklogs).click();
    }

    public void clickOnSearch() {

        driver.findElement(Search).click();
    }


    public void clickOnMyWorkLogs() {

        driver.findElement(MyWorkLogs).click();
    }

    public void clickOnEdit() {

        driver.findElement(Edit).click();
    }

    public void clickOnAdminButton() {

        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/ul/li/a/span"));
        action.moveToElement(we).moveToElement(driver.findElement(By.xpath("html/body/div[2]/div/div[2]/ul/li/ul/li[2]/a"))).click().build().perform();
    }

    public void clickOnManageOvertime() {

        driver.findElement(ManageOvertime).click();
    }

    public void clickOnApprove() {

        driver.findElement(Approve).click();
    }

    public void clickOnReject() {

        driver.findElement(Reject).click();
    }

    public void clickOnRejectConfirm() {

        driver.findElement(rejectConfirm).click();
    }

}