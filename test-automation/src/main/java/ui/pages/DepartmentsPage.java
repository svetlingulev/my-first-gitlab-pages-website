package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class DepartmentsPage {

    WebDriver driver;

    By department = By.xpath("//span[@class='title'][contains(text(),'Departments')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By Name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Departments']/tbody/tr[1]/td[2]/a[1]");
    By delete = By.xpath(".//*[@id='Departments']/tbody/tr[1]/td[2]/a[2]/span");
    By scheduler = By.id("CustomScheduler");
    By project = By.id("WorkOnProject");


    public DepartmentsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickDepartments() {

        driver.findElement(department).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void clickCustomScheduler() {

        driver.findElement(scheduler).click();
    }

    public void clickWorkOnProject() {

        driver.findElement(project).click();
    }

    public void editName() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1st department test - edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}