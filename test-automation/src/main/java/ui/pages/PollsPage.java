package ui.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ddzhonova on 09.11.16.
 */
public class PollsPage {

    WebDriver driver;

    By PollsButton = By.xpath("//span[@class='title'][contains(text(),'Polls')]");
    By AddPollsButton = By.xpath("//span[text()='Add new']");
    By Title = By.xpath(".//*[@id='Title']");
    By Answer = By.xpath(".//*[@id='Answer-1']");
    By Answer2 = By.xpath(".//*[@id='Answer-2']");
    By Result = By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/form/div/div[2]/div[7]/div/div/span/input");
    By Save = By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/form/div/div[4]/input");
    By Author = By.xpath(".//*[@id='Author']");
    By Search = By.xpath("//span[text()='Search']");
    By Delete = By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[10]/div[4]/a");
    By Edit = By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[10]/div[3]/button");
    By Vote = By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[10]/div[1]/button");
    By Vote2 = By.xpath("//input[@class='btn green btn-outline']");
    By BackToList = By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/form/div/div[5]/a");
    By Results = By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[10]/div[2]/button");

    public PollsPage(WebDriver driver){
        this.driver=driver;

    }


    public void clickOnPollsButton(){

        driver.findElement(PollsButton).click();

    }

    public void AddPollsButton(){
        driver.findElement(AddPollsButton).sendKeys(Keys.ENTER);

    }

    public void addPollDDValues(){

        WebElement PollType = driver.findElement(By.xpath(".//*[@id='Type']"));
        Select poll_dd = new Select(PollType);
        poll_dd.selectByVisibleText("Multi Answer");
    }

    public void typetitle(String name){

        driver.findElement(Title).sendKeys(name);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void addFromDate() throws InterruptedException {

        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

//        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));
//
//        int total_node = dates.size();
//
//        for(int i=0;i<total_node;i++){
//            String date=dates.get(i).getText();
//            if(date.equalsIgnoreCase("5")){
//                dates.get(i).click();
//                break;
//            }
//
//        }
    }

    public void addEndDtae(){

        driver.findElement(By.xpath(".//*[@id='datepickerEndBtn']")).click();
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

//        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));
//
//        int total_node = dates.size();
//
//        for(int i=0;i<total_node;i++){
//            String date=dates.get(i).getText();
//            if(date.equalsIgnoreCase("5")){
//                dates.get(i).click();
//                break;
//            }

//        }
    }

    public void addCompanyDDValues(){


        driver.findElement(By.xpath("(.//*[@id='usersAccordion']/span/div/button)[1]")).click();

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu) {
            el.click();
            break;

        }
        List<WebElement> dd_menu2=driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu2) {
            el.click();
            break;
        }

    }

    public void addCitiesDDValues() {

        driver.findElement(By.xpath("(.//*[@id='usersAccordion']/span/div/button)[2]")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("(//ul[@class='multiselect-container dropdown-menu']//li/a/label/input)"));

        for (WebElement el:dd_menu) {
            el.click();
            break;

        }
        List<WebElement> dd_menu2=driver.findElements(By.xpath("(//ul[@class='multiselect-container dropdown-menu']//li/a/label/input)"));

        for (WebElement el:dd_menu2) {
            el.click();
            break;
        }
      }

    public void answer1(String answer){

        driver.findElement(Answer).sendKeys(answer);

    }

    public void answer2(String answer){

        driver.findElement(Answer2).sendKeys(answer);

    }

    public void clickonResultIsPublicButton(){

        driver.findElement(Result).click();

    }

    public void clickonSaveButton(){

        driver.findElement(Save).click();

    }





    ///List Polls

    public void addFromDtaeListPolls() {

        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

//        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));
//
//        int total_node = dates.size();
//
//        for (int i = 0; i < total_node; i++) {
//            String date = dates.get(i).getText();
//            if (date.equalsIgnoreCase("30")) {
//                dates.get(i).click();
//                break;
//            }
//
//        }
    }

    public void Title (String title){

        driver.findElement(Title).sendKeys(title);

    }

    public void Author (String author){

        driver.findElement(Author).sendKeys(author);

    }

    public void selectDDStatus() {

        WebElement Status = driver.findElement(By.id("Status"));
        Select status_dd = new Select(Status);
        status_dd.selectByVisibleText("All");

    }
    public void clickonSearchButton(){

        driver.findElement(Search).click();

    }

    public void clickonDeleteButton(){

        driver.findElement(Delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }

    public void clickonEditButton(){

        driver.findElement(Edit).click();

    }

    public void clickonVoteButton() {

        driver.findElement(Vote).sendKeys(Keys.ENTER);

    }

    public void checkVote(){

        driver.findElement(By.xpath("(.//*[@id='uniform-UserVotes']/span/input)[1]")).click();
        driver.findElement(By.xpath("(.//*[@id='uniform-UserVotes']/span/input)[2]")).click();

        driver.findElement(Vote2).sendKeys(Keys.ENTER);
//        driver.findElement(BackToList).click();
//        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
//        Assert.assertEquals(Text, "Thank you for voting.");
    }

    public void clickonBackToList() {

        driver.findElement(BackToList).sendKeys(Keys.ENTER);
        driver.findElement(Results).sendKeys(Keys.ENTER);

    }




}
