package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class HolidayPage {

    WebDriver driver;

    By configuration = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[10]/a/span[1]");
    By holiday = By.xpath(".//*[@id='administration']/li[7]/a/span");
    By addNew = By.xpath("//span[text()='Add new']");
    By holidayName = By.id("HolidayName");
    By sync = By.id("IsSynchronizable");
    By save = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/input");
    By edit = By.xpath(".//*[@id='Holidays']/tbody/tr[3]/td[4]/a[1]");
    By delete = By.xpath(".//*[@id='Holidays']/tbody/tr[3]/td[4]/a[2]");


    public HolidayPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickConfiguration() {

        driver.findElement(configuration).click();
    }

    public void clickHoliday() {

        driver.findElement(holiday).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeHolidayName(String holiday) {

        driver.findElement(holidayName).sendKeys(holiday);
    }

    public void holidayDate() {
        driver.findElement(By.cssSelector("button.btn.default")).click();
        driver.findElement(By.cssSelector("td.active.day")).click();
    }

    public void syncWithGoogleCalendar() {

        driver.findElement(sync).click();
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void editHolidayName() {

        driver.findElement(holidayName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "editedHoliday");
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }

}