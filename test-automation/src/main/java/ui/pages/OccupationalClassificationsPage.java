package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class OccupationalClassificationsPage {

    WebDriver driver;

    By occupationalClassification = By.xpath("//span[@class='title'][contains(text(),'Occupational Classifications')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By Name = By.id("Name");
    By code = By.xpath(".//*[@id='Code']");
    By description = By.xpath(".//*[@id='Description']");
    By socialSecurity = By.xpath(".//*[@id='MinSocialSecurityThreshold']");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By search = By.xpath("//button[@class='btn blue btn-outline']");
    By edit = By.xpath("//span[text()='Edit']");
    By delete = By.xpath("//span[text()='Delete']");

    public OccupationalClassificationsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickOccupationalClassification() {

        driver.findElement(occupationalClassification).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void clickSearch() {

        driver.findElement(search).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void typeCode(String name) {

        driver.findElement(code).sendKeys(name);
    }

    public void typeDescription(String name) {

        driver.findElement(description).sendKeys(name);
    }

    public void typeSocialSecurity() {

        driver.findElement(socialSecurity).sendKeys(Keys.chord(Keys.CONTROL, "a"), "150,55");
    }

    public void editName() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "Edited test classification");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void companyDD() {

        WebElement company = driver.findElement(By.xpath(".//*[@id='CompanyId']"));
        Select company_dd = new Select(company);
        company_dd.selectByVisibleText("Prime");
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}