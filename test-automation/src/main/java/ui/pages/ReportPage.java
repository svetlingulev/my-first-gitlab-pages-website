package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ReportPage {

    WebDriver driver;

    By report = By.xpath(".//*[@id='administration']/li[6]/a/span[1]");
    By templates = By.xpath(".//*[@id='query']/li[1]/a/span");
    By addnew = By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[2]/div/a/span");
    By name = By.xpath(".//*[@id='Name']");
    By save = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/input");
    By edit = By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[9]/a[1]/span");
    By createReport = By.xpath(".//*[@id='query']/li[2]/a/span");
    By generate = By.xpath(".//*[@id='create']");
    By query = By.xpath(".//*[@id='query']/li[3]/a/span");
    By queryGenerate = By.xpath("html/body/div[4]/div[2]/div/div/div[3]/form/div[2]/button[1]");
    By reportName = By.xpath(".//*[@id='TableName']");
    By psTimesheet = By.xpath(".//*[@id='query']/li[5]/a/span");
    By sheetsReport = By.xpath(".//*[@id='Sheeted']");
    By timesheetGenerate = By.name("action");
    By form76Generate = By.xpath("html/body/div[4]/div[2]/div/div/div[3]/form/div[2]/button[1]");
    By form76 = By.xpath("//span[@class='title'][contains(text(),'Form 76')]");
    By psMonthReport = By.xpath("//span[@class='title'][contains(text(),'PS Month Report')]");


    public ReportPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnReport(){

        driver.findElement(report).click();
    }

    public void clickOnTemplates(){

        driver.findElement(templates).click();
    }

    public void clickOnAddNew(){

        driver.findElement(addnew).click();
    }

    public void typeName(String addName){

        driver.findElement(name).sendKeys(addName);
    }

    public void typeDD() {

        WebElement type = driver.findElement(By.xpath(".//*[@id='TemplateType']"));
        Select type_dd = new Select(type);
        type_dd.selectByVisibleText("Combined");
        Assert.assertEquals(type_dd.getFirstSelectedOption().getText(), "Combined");
    }

    public void addTeamsDD() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[1]/div/div[2]/div[1]/span/div/button")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu) {
            el.click();
            break;
        }
    }

    public void addUserProperties() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[1]/div/div[3]/div[1]/span[2]/div/button")).sendKeys(Keys.ENTER);

        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("//span[2]/div/ul/li[2]/a/label")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();

    }

    public void addProjects(){

        driver.findElement(By.xpath("(//button[@type='button'])[9]")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(//button[@type='button'])[9]")).sendKeys(Keys.ENTER);
    }

    public void addDepartment() {

        driver.findElement(By.xpath("(//button[@type='button'])[3]")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).sendKeys(Keys.ENTER);
    }

    public void addCustomUserProperties() {

        driver.findElement(By.xpath("(//button[@type='button'])[7]")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).sendKeys(Keys.ENTER);

    }

    public void addClients() {

        driver.findElement(By.xpath("(//button[@type='button'])[11]")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(//button[@type='button'])[11]")).sendKeys(Keys.ENTER);
    }

    public void addFunctionalities() {

        driver.findElement(By.xpath("(//button[@type='button'])[13]")).sendKeys(Keys.ENTER);
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(//button[@type='button'])[13]")).sendKeys(Keys.ENTER);
    }

    public void clickSave(){

        driver.findElement(save).click();
    }

    public void clickEdit(){

        driver.findElement(edit).click();
    }

    public void typeEditedName() {
        driver.findElement(name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1edited name");
    }

    public void startDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerStart']/span/button")).sendKeys(Keys.ENTER);
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));
        int total_node = dates.size();
        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("1")) {
                dates.get(i).click();
                break;
            }
        }
    }

    public void endDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerEnd']/span/button")).sendKeys(Keys.ENTER);
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));
        int total_node = dates.size();
        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("1")) {
                dates.get(i).click();
                break;
            }
        }
    }

    public void periodDD() {

        WebElement type = driver.findElement(By.xpath(".//*[@id='AuthType']"));
        Select type_dd = new Select(type);
        type_dd.selectByVisibleText("Days");
        Assert.assertEquals(type_dd.getFirstSelectedOption().getText(), "Days");
    }

    public void templatesDD(){

        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
        driver.findElement(By.cssSelector("input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
    }

    public void clickCreateReport(){

        driver.findElement(createReport).click();
    }

    public void clickGenerate(){

        driver.findElement(generate).click();
    }

    public void clickQuery(){

        driver.findElement(query).click();
    }

    public void queryTypeDD() {

        WebElement queryType = driver.findElement(By.xpath(".//*[@id='Types']"));
        Select type_dd = new Select(queryType);
        type_dd.selectByVisibleText("Combined");
        Assert.assertEquals(type_dd.getFirstSelectedOption().getText(), "Combined");
    }

    public void queryTeams() {

        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
        driver.findElement(By.cssSelector("input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
    }

    public void queryDepartments() {

        driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();
    }

    public void queryUserProperties() {

        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();

    }

    public void queryCustomProperties(){

        driver.findElement(By.xpath("(//button[@type='button'])[9]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[9]")).click();
    }

    public void queryProjects(){

        driver.findElement(By.xpath("(//button[@type='button'])[11]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[11]")).click();

    }

    public void queryClients(){

        driver.findElement(By.xpath("(//button[@type='button'])[13]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[13]")).click();

    }

    public void queryFunctionality(){

        driver.findElement(By.xpath("(//button[@type='button'])[15]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("div.btn-group.open > ul.multiselect-container.dropdown-menu > li.multiselect-item.multiselect-all > a.multiselect-all > label.checkbox > input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[15]")).click();

    }

    public void clickQueryGenerate(){

        driver.findElement(queryGenerate).click();
    }

    public void toDate() {
        driver.findElement(By.xpath(".//*[@id='StartDate']/span/button")).sendKeys(Keys.ENTER);
//        driver.findElement(By.xpath(" //td[@class='active day']")).click();

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("1")) {
                dates.get(i).click();
                break;
            }

//            driver.findElement(By.xpath(".//*[@id='StartDate']/span/button")).sendKeys(Keys.ENTER);
//            driver.findElement(By.xpath(" //td[@class='active day']")).click();
        }
    }

    public void fromDate() {
        driver.findElement(By.xpath(".//*[@id='EndDate']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();
    }

    public void typeReportName() {

        driver.findElement(reportName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "PS Timesheet");
    }

    public void teamDD() {

        driver.findElement(By.xpath(".//*[@id='select2-TeamID-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("//span[@class='select2-dropdown select2-dropdown--below']//li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Inferno")) {
                element.click();
                break;
            }
        }
    }

    public void clickPsTimesheet(){

        driver.findElement(psTimesheet).click();
    }

    public void clickOnSheetsReport(){

        driver.findElement(sheetsReport).click();

    }

    public void sheetedBy() {

        WebElement sheet = driver.findElement(By.xpath(".//*[@id='Employee']"));
        Select sheet_dd = new Select(sheet);
        sheet_dd.selectByVisibleText("Employee");
        Assert.assertEquals(sheet_dd.getFirstSelectedOption().getText(), "Employee");
    }

    public void clickTimesheetGenerate(){

        driver.findElement(timesheetGenerate).click();
    }

    public void companiesDD() {

        WebElement company = driver.findElement(By.xpath(".//*[@id='CompanyID']"));
        Select company_dd = new Select(company);
        company_dd.selectByVisibleText("Prime");
        Assert.assertEquals(company_dd.getFirstSelectedOption().getText(), "Prime");
    }

    public void month() {

        driver.findElement(By.xpath(".//*[@id='Date']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //span[@class='month active']")).click();
    }

    public void clickForm76Generate(){

        driver.findElement(form76Generate).click();
    }

    public void clickForm76(){

        driver.findElement(form76).click();
    }

    public void clickPsMonthReport(){

        driver.findElement(psMonthReport).click();
    }

    public void toOldDate() {

        driver.findElement(By.cssSelector("button.btn.default")).click();
        driver.findElement(By.cssSelector("th.prev")).click();

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("1")) {
                dates.get(i).click();
                break;
            }

        }
    }

    public void fromOldDate() {

        driver.findElement(By.cssSelector("#EndDate > span.input-group-btn > button.btn.default")).click();
        driver.findElement(By.cssSelector("th.prev")).click();

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("31")) {
                dates.get(i).click();
                break;
            }

        }
    }

    public void usersDD() {

        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
        driver.findElement(By.cssSelector("input[type=\"checkbox\"]")).click();
        driver.findElement(By.cssSelector("input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
    }


}