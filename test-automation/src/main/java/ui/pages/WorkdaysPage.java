package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorkdaysPage {

    WebDriver driver;

    By workdays = By.xpath(".//*[@id='administration']/li[8]/a/span");
    By addNew = By.xpath("//span[text()='Add new']");
    By workdayName = By.id("WorkDayName");
    By synch = By.id("IsSynchronizable");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath("//a[@class='btn green-meadow btn-outline']");
    By delete = By.xpath("//a[@class='btn red btn-outline']");



    public WorkdaysPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickWorkdays() {

        driver.findElement(workdays).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeWorkdayName(String holiday) {

        driver.findElement(workdayName).sendKeys(holiday);
    }

    public void workdayDate(){

        driver.findElement(By.id("WorkDayDate")).click();
        driver.findElement(By.cssSelector("td.active.day")).click();
    }

    public void clickSynchronize() {

        driver.findElement(synch).click();
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void findByYearDD() {

        WebElement year = driver.findElement(By.id("WorkDayDate"));
        Select year_dd = new Select(year);
        year_dd.selectByVisibleText("2017");
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void editedName() {

            driver.findElement(workdayName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "editedWorkday");
        }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}