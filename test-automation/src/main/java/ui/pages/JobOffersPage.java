package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class JobOffersPage {

    WebDriver driver;

    By AddNew = By.xpath("//span[text()='Add new']");
    By Title = By.xpath("//input[@id='title']");
    By Description = By.xpath(".//*[@id='description']");
    By Content = By.xpath(".//div[@class='note-editable panel-body']");
    By Sticky = By.xpath(".//*[@id='edit-jobOffer-form']/div[2]/div[1]/div[5]/div/div/span/input");
    By Active = By.xpath(".//*[@id='isactive']");
    By Save = By.xpath(".//*[@id='send']");
    By JobOffers = By.xpath("//span[@class='title'][contains(text(),'Job Offers')]");
    By ShortDescriptuin = By.xpath(".//*[@id='Description']");
    By Search = By.xpath("//span[text()='Search']");
    By Title2 = By.xpath("//input[@id='Title']");
    By Edit = By.xpath("//span[text()='Edit']");
    By Delete = By.xpath("//span[text()='Delete']");
    By DeleteConfirm = By.xpath(".//*[@id='delete-confirm']");


    public JobOffersPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickJobOffers() {

        driver.findElement(JobOffers).click();
    }

    public void clickAddNewButton() {

        driver.findElement(AddNew).click();
    }

    public void typeTitle(String title) {

        driver.findElement(Title).sendKeys(title);
    }

    public void addTargetCompanies() {

        driver.findElement(By.xpath(".//*[@id='edit-jobOffer-form']/div[2]/div[1]/div[1]/div[2]/span/div/button")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el : dd_menu) {
            el.click();
           break;
        }
        List<WebElement> dd_menu2 = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el : dd_menu2) {
            el.click();
            break;
        }

    }

    public void typeDescription(String description) {

        driver.findElement(Description).sendKeys(description);
    }

    public void typeContent(String content) {

        driver.findElement(By.xpath(".//*[@id='edit-jobOffer-form']/div[2]/div[1]/div[4]/div/div/div[2]/div[1]/button[1]")).click();
        driver.findElement(Content).sendKeys(content);
    }


    public void clickIsSticky() {

        driver.findElement(Sticky).click();
    }

    public void clickIsActive() {

        driver.findElement(Active).click();
    }

    public void clickSave() {

        driver.findElement(Save).click();
    }


    public void fromDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerFromDate']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='today day']")).click();

    }

    public void toDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerToDate']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='today day']")).sendKeys(Keys.ENTER);

    }

    public void typeTitle2(String title) {

        driver.findElement(Title2).sendKeys(title);
    }

    public void typeShortDescription(String description) {

        driver.findElement(ShortDescriptuin).sendKeys(description);
    }

    public void selectStatus() {

        WebElement Status = driver.findElement(By.id("Status"));
        Select status_dd = new Select(Status);
        status_dd.selectByVisibleText("All");
        Assert.assertEquals(status_dd.getFirstSelectedOption().getText(), "All");
    }

    public void clickSearch() {

        driver.findElement(Search).click();
    }

    public void clickEdit() {

        driver.findElement(Edit).click();
    }

    public void editTitle(){

        driver.findElement(Title).sendKeys(Keys.chord(Keys.CONTROL, "a"), "editedJob");
    }

    public void clickDelete() {

        driver.findElement(Delete).click();
    }

    public void clickDeleteConfirm() {

        driver.findElement(DeleteConfirm).click();
    }


}