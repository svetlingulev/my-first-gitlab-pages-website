package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class TimeOffPage {

    WebDriver driver;

    By request = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[1]/a/span");
    By somewhere = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]");
    By halfDay = By.xpath(".//*[@id='IsHalfDay']");
    By submit = By.xpath(".//*[@id='modalWindow']");
    By timeOff = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/a/span[1]");
    By confirm = By.xpath(".//*[@id='request-confirm']");
    By myRequests = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[2]/a/span");
    By manageRequests = By.xpath("//span[@class='title'][contains(text(), 'Manage Requests')]");
    By approve = By.xpath("//i[@class='fa fa-thumbs-up'][1]");
    By delete2 = By.xpath("(//i[@class='fa fa-trash-o'])[1]");
    By deleteConfirm = By.xpath(".//*[@id='delete-confirm']");
    By approveDelete = By.xpath(".//*[@id='pending ptos']/tbody/tr/td[7]/a[1]");
    By reject = By.xpath("(//i[@class='fa fa-thumbs-down'])[1]");
    By comment = By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[8]/a/span[1]");
    By typeComment = By.xpath(".//*[@id='commentText']");
    By submitComment= By.xpath(".//*[@id='comments']/div[3]/div[2]/button");
    By back = By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[2]/div/div/button");
    By allTimeOff = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[5]/a/span");
    By search = By.xpath(".//*[@id='formItems']/div[3]/div/button[1]");
    By approvedTimeOff = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[4]/a/span");
    By search2 = By.xpath("html/body/div[4]/div[2]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div/div/button[1]");
    By requestReport = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[6]/a/span");
    By generate = By.xpath(".//*[@id='search']");
    By groups = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[7]/a/span");
    By newGroup = By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[2]/div/div/a/span");
    By groupName = By.xpath(".//*[@id='Name']");
    By allowance = By.xpath(".//*[@id='Allowance']");
    By description = By.xpath(".//*[@id='Description']");
    By notify = By.xpath(".//*[@id='NotifyTeamMembers']");
    By saveGroup = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/input");
    By editGroup = By.xpath(".//*[@id='TimeOffGroups']/tbody/tr[5]/td[9]/a[1]");
    By deleteGroup = By.xpath(".//*[@id='TimeOffGroups']/tbody/tr[5]/td[9]/a[2]");
    By userDaysOff = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[13]/ul/li[8]/a/span");
    By daysOffSearch = By.xpath("html/body/div[4]/div[2]/div/div/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div/button[1]");



    public TimeOffPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickTimeOff() {
        driver.findElement(timeOff).click();
    }

    public void clickRequest() {
        driver.findElement(request).click();
    }

    public void clickSomewhere() {
        driver.findElement(somewhere).click();
    }

    public void addTimeOffgroup() {

        WebElement timeOffgroup = driver.findElement(By.xpath(".//*[@id='UserTimeOffGroupID']"));
        Select timeoff_dd = new Select(timeOffgroup);
        timeoff_dd.selectByVisibleText("Special time off");
    }

    public void startDate() {

        driver.findElement(By.id("datepickerStartBtn")).click();
        driver.findElement(By.cssSelector("td.active.day")).click();
    }

    public void endDate() {

        driver.findElement(By.id("datepickerEndBtn")).click();
        driver.findElement(By.cssSelector("td.active.day")).click();
    }

    public void endDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerEndBtn']")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }
        }
    }

    public void startDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }
        }
    }

    public void startDateWeekend() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='day weekend']")).click();
    }

    public void endDateWeekend() {
        driver.findElement(By.xpath("//button[@id='datepickerEndBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day weekend']")).click();
    }

    public void clickHalfDay() {
        driver.findElement(halfDay).click();
    }

    public void clickSubmit() {
        driver.findElement(submit).click();
    }

    public void clickConfirm() {
        driver.findElement(confirm).click();
    }

    public void clickMyRequests() {
        driver.findElement(myRequests).click();
    }

    public void clickOnDeleteButton() throws InterruptedException {

        WebElement element = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[9]/a"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.xpath(".//*[@id='delete-confirm']"));
        if (element2.isDisplayed()) {
            element2.click();
        }
    }

    public void clickMnaganeRequests() {
        driver.findElement(manageRequests).click();
    }

    public void clickApprove(){
        driver.findElement(approve).click();
    }

    public void clickDelete2(){
        driver.findElement(delete2).click();
    }
    public void clickdeleteComfirm(){
        driver.findElement(deleteConfirm).click();
    }

    public void clickApproveDelete(){
        driver.findElement(approveDelete).click();
    }

    public void clickReject(){
        driver.findElement(reject).click();
    }

    public void clickComment(){
        driver.findElement(comment).click();
    }

    public void typeComment(String comment){
        driver.findElement(typeComment).sendKeys(comment);
    }

    public void clickSubmitComment(){
        driver.findElement(submitComment).click();
    }

    public void clickBack(){
        driver.findElement(back).click();
    }

    public void clickAllTineOff(){
        driver.findElement(allTimeOff).click();
    }

    public void selectTimeOffgroup() {

        WebElement selectTimeOffgroup = driver.findElement(By.xpath(".//*[@id='SelectedTimeOffGroupId']"));
        Select selectTimeoff_dd = new Select(selectTimeOffgroup);
        selectTimeoff_dd.selectByVisibleText("Special time off");
    }

    public void selectUser() {

        driver.findElement(By.xpath(".//*[@id='select2-SelectedUserId-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath(".//*[@id='select2-SelectedUserId-results']/li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Admin Admin")) {
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void clickSearchBtn(){
        driver.findElement(search).click();
    }
    public void clickApprovedRequests(){
        driver.findElement(approvedTimeOff).click();
    }

    public void selectTimeOffgroup2() {

        WebElement selectTimeOffgroup = driver.findElement(By.xpath(".//*[@id='GroupId']"));
        Select selectTimeoff_dd = new Select(selectTimeOffgroup);
        selectTimeoff_dd.selectByVisibleText("Special time off");
    }

    public void selectUser2() {

        driver.findElement(By.xpath(".//*[@id='select2-UserId-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath(".//*[@id='select2-UserId-results']/li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Admin Admin")) {
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void clickSearch2(){
        driver.findElement(search2).click();
    }

    public void clickRequestReport() {
        driver.findElement(requestReport).click();
    }

    public void reportStartDay() {
        driver.findElement(By.xpath(".//*[@id='DateOfReport']/span/button")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

    }

    public void addRequestReportDD() {

        WebElement timeOffgroup = driver.findElement(By.xpath(".//*[@id='GroupId']"));
        Select group_dd = new Select(timeOffgroup);
        group_dd.selectByVisibleText("Special time off");

        WebElement company = driver.findElement(By.xpath(".//*[@id='CompanyId']"));
        Select company_dd = new Select(company);
        company_dd.selectByVisibleText("Prime");

        WebElement userStatus = driver.findElement(By.xpath(".//*[@id='UserStatus']"));
        Select status_dd = new Select(userStatus);
        status_dd.selectByVisibleText("All");
    }

    public void clickGenerate() {
        driver.findElement(generate).click();
    }

    public void clickGroups() {
        driver.findElement(groups).click();
    }

    public void addNewgroup() {
        driver.findElement(newGroup).click();
    }

    public void typeGroupName(String gname){
        driver.findElement(groupName).sendKeys(gname);
    }

    public void typeAnnualAllowance() {
        driver.findElement(allowance).sendKeys(Keys.chord(Keys.CONTROL, "a"), "8");
    }

    public void addApprover() {

        WebElement approver = driver.findElement(By.xpath(".//*[@id='countryList']"));
        Select approver_dd = new Select(approver);
        approver_dd.selectByVisibleText("Admin Admin");
    }

    public void typeDescription(String add_description){
        driver.findElement(description).sendKeys(add_description);
    }

    public void clickNotify() {
        driver.findElement(notify).click();
    }

    public void clickSaveGroup() {
        driver.findElement(saveGroup).click();
    }

    public void clickEditGroup() {
        driver.findElement(editGroup).click();
    }

    public void editDescription() {
        driver.findElement(description).sendKeys(Keys.chord(Keys.CONTROL, "a"), "edited test");
    }

    public void clickDeleteGroup() {
        driver.findElement(deleteGroup).click();
    }

    public void userDD() {

        driver.findElement(By.xpath(".//*[@id='select2-FilterUserId-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath(".//*[@id='select2-FilterUserId-results']/li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Admin Admin")) {
                element.click();
                break;
            }
        }
    }

    public void companyDD() {

        WebElement company = driver.findElement(By.xpath(".//*[@id='FilterCompanyId']"));
        Select company_dd = new Select(company);
        company_dd.selectByVisibleText("Prime");
    }

    public void timeOffGroupDD() {

        WebElement timeOffGroup = driver.findElement(By.xpath(".//*[@id='GroupId']"));
        Select timeOffGroup_dd = new Select(timeOffGroup);
        timeOffGroup_dd.selectByVisibleText("Special time off");
    }

    public void yearDD() {

        WebElement year = driver.findElement(By.xpath(".//*[@id='Year']"));
        Select year_dd = new Select(year);
        year_dd.selectByVisibleText("All");
    }

    public void userStatusDD() {

        WebElement userStatus = driver.findElement(By.xpath(".//*[@id='UserStatus']"));
        Select userStatus_dd = new Select(userStatus);
        userStatus_dd.selectByVisibleText("Active");
    }

    public void clickUsersDaysOff() {
        driver.findElement(userDaysOff).click();
    }

    public void clickSearch() {
        driver.findElement(daysOffSearch).click();
    }



}