package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorkFromHomePage {

    WebDriver driver;

    By workFromHome = By.xpath("//span[@class='title'][contains(text(), 'Work From Home')]");
    By request = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[14]/ul/li[1]/a/span");
    By hours = By.xpath(".//*[@id='WFHTime']");
    By saveRequest = By.xpath(".//*[@id='modalWindow']");
    By calendar = By.xpath("//button[@id='datepickerEndBtn']");
    By somewhere = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]");
    By confirm = By.xpath(".//*[@id='request-confirm']");
    By manageReguests = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[14]/ul/li[3]/a/span");
    By myRequest = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[14]/ul/li[2]/a/span");
    By approve = By.xpath(".//*[@id='pending ptos']/tbody/tr[1]/td[6]/a[1]/span");


    public WorkFromHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickWorkFromHome() {

        driver.findElement(workFromHome).click();
    }

    public void clickRequets() {

        driver.findElement(request).click();
    }

    public void startDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

    }

    public void endDate() {
        driver.findElement(By.xpath("//button[@id='datepickerEndBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day']")).click();

    }

    public void endDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerEndBtn']")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }

        }
    }

    public void startDate2() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).click();
        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }

        }

    }

    public void typeLoggedHours() {

        driver.findElement(hours).sendKeys(Keys.chord(Keys.CONTROL, "a"), "8");

    }

    public void clickSaveRequest() {

        driver.findElement(saveRequest).click();
    }

    public void clickSomewhere() {

        driver.findElement(somewhere).click();
    }

    public void clickConfirmRequest() {

        driver.findElement(confirm).click();
    }

    public void startDateWeekend() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='day weekend']")).click();
    }

    public void endDateWeekend() {
        driver.findElement(By.xpath("//button[@id='datepickerEndBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath(" //td[@class='active day weekend']")).click();
    }

    public void clickManageRequests() {

        driver.findElement(manageReguests).click();
    }

    public void clickMyRequest() {

        driver.findElement(myRequest).click();
    }

    public void clickOnDeleteButton() throws InterruptedException {

        WebElement element = driver.findElement(By.xpath("//a[@class='btn red btn-outline']/i[@class='fa fa-trash-o']"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.xpath("//a[@id='delete-confirm']/i[@class='fa fa-trash-o']"));
        if (element2.isDisplayed()) {
            element2.click();
        }
    }

    public void clickApprove() {

        driver.findElement(approve).click();
    }


}