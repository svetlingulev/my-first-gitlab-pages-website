package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CustomUserFieldsPage {

    WebDriver driver;

    By name = By.xpath(".//*[@id='Name']");
    By labelName = By.xpath(".//*[@id='LabelName']");
    By save = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[3]/input");
    By customUser = By.xpath("//span[text()='Custom User Fields']");
    By addNew = By.xpath("//span[text()='Add new']");
    By search = By.xpath("//button[@class='btn blue btn-outline']");
    By edit = By.xpath("//span[text()='Edit']");
    By delete = By.xpath("//span[text()='Delete']");
    By deleteConfirm = By.xpath(".//*[@id='delete-confirm']");

    public CustomUserFieldsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void typeName(String addName){
        driver.findElement(name).sendKeys(addName);
    }

    public void typeLabelName(String addLabel){
        driver.findElement(labelName).sendKeys(addLabel);
    }

    public void attributeTypeDD() {

        WebElement type = driver.findElement(By.xpath(".//*[@id='CustomAttributeType']"));
        Select type_dd = new Select(type);
        type_dd.selectByVisibleText("String");
        Assert.assertEquals(type_dd.getFirstSelectedOption().getText(), "String");
    }

    public void listAttributeTypeDD() {

        WebElement type = driver.findElement(By.xpath(".//*[@id='AttributeType']"));
        Select type_dd = new Select(type);
        type_dd.selectByVisibleText("String");
        Assert.assertEquals(type_dd.getFirstSelectedOption().getText(), "String");
    }

    public void clickOnSave(){
        driver.findElement(save).click();
    }

    public void clickOnCustomUser(){
        driver.findElement(customUser).click();
    }

    public void clickOnAddNew(){
        driver.findElement(addNew).click();
    }

    public void clickOnSearch(){
        driver.findElement(search).click();
    }

    public void clickOnEdit(){
        driver.findElement(edit).click();
    }

    public void typeEditedName() {
        driver.findElement(name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "edited name");
    }

    public void clickOnDelete() throws InterruptedException {
        driver.findElement(delete).click();
        Thread.sleep(1000);
        driver.findElement(deleteConfirm).click();
    }

}