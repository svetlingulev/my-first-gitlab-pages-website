package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class FeedPage {

    WebDriver driver;

    By AddNew = By.xpath("//span[text()='Add new']");
    By Title = By.xpath(".//*[@id='title']");
    By Description = By.xpath(".//*[@id='description']");
    By Content = By.xpath(".//div[@class='note-editable panel-body']");
    By Sticky = By.xpath(".//*[@id='edit-newsFeeds-form']/div[2]/div[1]/div[5]/div/div/span/input");
    By Active = By.xpath(".//*[@id='edit-newsFeeds-form']/div[2]/div[1]/div[6]/div/div/span/input");
    By Save = By.xpath(".//*[@id='edit-newsFeeds-form']/div[2]/div[2]/input");
    By Author = By.xpath(".//*[@id='Author']");
    By Title2 = By.xpath(".//*[@id='Title']");
    By Search = By.xpath("//span[text()='Search']");
    By Delete = By.xpath("//span[text()='Delete']");
    By Edit = By.xpath("//span[text()='Edit']");
    By NewsFeed = By.xpath("//span[@class='title'][contains(text(),'News Feed')]");
    By saveAndSend = By.xpath(".//*[@id='sendemail']");


    public FeedPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickAddNewButton() {

        driver.findElement(AddNew).click();

    }

    public void clickNewsFeed() {

        driver.findElement(NewsFeed).click();

    }

    public void addActiveDate() {

        driver.findElement(By.xpath(".//*[@id='datepickerPostDate']/span/button")).sendKeys(Keys.ENTER);
//        driver.findElement(By.xpath(" //td[@class='active day']")).click();

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("30")) {
                dates.get(i).click();
                break;
            }

        }
    }


    public void typeTitle(String title) {

        driver.findElement(Title).sendKeys(title);
    }

    public void addTargetCompanies() {

        driver.findElement(By.xpath(".//*[@id='edit-newsFeeds-form']/div[2]/div[1]/div[2]/div[2]/span/div/button")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el : dd_menu) {
            el.click();
            break;

        }
        List<WebElement> dd_menu2 = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el : dd_menu2) {
            el.click();
            break;
        }

    }

    public void typeDescription(String description) {

        driver.findElement(Description).sendKeys(description);

    }
    public void typeContent(String content) {

        driver.findElement(By.xpath(".//*[@id='edit-newsFeeds-form']/div[2]/div[1]/div[4]/div/div/div[2]/div[1]/button[1]")).click();
        driver.findElement(Content).sendKeys(content);

    }

    public void clickIsSticky() {

        driver.findElement(Sticky).click();

    }

    public void clickIsActive() {

        driver.findElement(Active).click();

    }

    public void clickSave() {

        driver.findElement(Save).click();

    }

    public void fromDate() {

        driver.findElement(By.xpath(".//*[@id='datepickerFromDate']/span/button")).sendKeys(Keys.ENTER);

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("30")) {
                dates.get(i).click();
                break;
            }

        }
    }

    public void toDate() {

        driver.findElement(By.xpath(".//*[@id='datepickerToDate']/span/button")).sendKeys(Keys.ENTER);

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("30")) {
                dates.get(i).click();
                break;
            }

        }
    }

    public void typeAuthor(String author) {

        driver.findElement(Author).sendKeys(author);
    }

    public void typeTitle2(String title) {

        driver.findElement(Title2).sendKeys(title);
    }

    public void selectStatus() {

        WebElement Status = driver.findElement(By.id("Status"));
        Select status_dd = new Select(Status);
        status_dd.selectByVisibleText("All");
        Assert.assertEquals(status_dd.getFirstSelectedOption().getText(), "All");
    }

    public void clickSearch() {

        driver.findElement(Search).click();

    }
    public void clickDelete() throws InterruptedException {

//        driver.findElement(Delete).click();
//        WebDriverWait wait = new WebDriverWait(driver, 5);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

        WebElement element = driver.findElement(By.xpath("//span[text()='Delete']"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.xpath("//a[@class='btn btn-danger']/i[@class='fa fa-trash-o']"));
        if (element2.isDisplayed()) {
            element2.click();

        }
    }
    public void clickEdit() {

        driver.findElement(Edit).click();

    }
    public void editTitle() {

        driver.findElement(Title).sendKeys(Keys.chord(Keys.CONTROL, "a"), "edited feed");
    }

    public void clickSaveAndSend() {

        driver.findElement(saveAndSend).click();
    }

}