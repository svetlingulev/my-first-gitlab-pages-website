package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by ddzhonova on 16.11.16.
 */
public class ProjectsPage {

    WebDriver driver;

    By projetcs = By.xpath("//span[@class='title'][contains(text(),'Projects')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By projectsName = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Projects']/tbody/tr[1]/td[10]/div/a");
    By editMenu = By.xpath(".//*[@id='Projects']/tbody/tr[1]/td[10]/div/ul/li[1]/a");

    By delete = By.xpath(".//*[@id='Projects']/tbody/tr[1]/td[10]/div/a");
    By deleteMenu = By.xpath(".//*[@id='Projects']/tbody/tr[1]/td[10]/div/ul/li[7]/a");
    By confirmDeletion = By.id("delete-confirm");

    By jiraProjectsName = By.xpath(".//*[@id='JiraProjectName']");
    By businessValueControl = By.xpath(".//*[@id='ProjectOverview']");
    //By isBillable = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div/div/div[6]/div/span/input");
    By sortByName = By.xpath(".//*[@id='Projects']/thead/tr[1]/th[1]/input");

    By modalProjectName = By.id("create-name");
    By modalProjectType = By.id("LinkOption");
    By modalCreateButton = By.id("create-button");



    public ProjectsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void sortProjectsByName() {

        driver.findElement(sortByName).click();
    }

    public void clickProjects() {

        driver.findElement(projetcs).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeProjectsName(String name) {

        driver.findElement(projectsName).sendKeys(name);
    }

    public void typeJiraProjectsName(String jiraName) {

        driver.findElement(jiraProjectsName).sendKeys(jiraName);
    }
    public void modalNewProject(String name) {

        driver.findElement(modalProjectName).sendKeys(name);
        //Select dropdown = new Select(driver.findElement(modalProjectType));
        //dropdown.selectByIndex(0);
        driver.findElement(modalCreateButton).click();

    }

    public void editProjectsName() {

        driver.findElement(projectsName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1st projects test - edited");
    }

    public void clientsDD() {

        WebElement client = driver.findElement(By.xpath(".//*[@id='ClientID']"));
        Select client_dd = new Select(client);
        client_dd.selectByIndex(1);
    }

    public void branchDD() {

        WebElement branch = driver.findElement(By.xpath(".//*[@id='BranchID']"));
        Select branch_dd = new Select(branch);
        branch_dd.selectByIndex(1);
    }
    public void directorOfProductionDD() {

        WebElement directorOfProduction = driver.findElement(By.xpath(".//*[@id='DirectorOfProductionID']"));
        Select directorOfProduction_dd = new Select(directorOfProduction);
        directorOfProduction_dd.selectByIndex(1);

    }
    public void productionManagerDD() {

        WebElement productionManager = driver.findElement(By.xpath(".//*[@id='ProductionManagerID']"));
        Select productionManager_dd = new Select(productionManager);
        productionManager_dd.selectByIndex(1);
    }
    public void projectManagerDD() {

        WebElement projectManager = driver.findElement(By.xpath(".//*[@id='ProjectManagerID']"));
        Select projectManager_dd = new Select(projectManager);
        projectManager_dd.selectByIndex(1);
    }

    public void typeBusinessValue(String businessValue) {

        driver.findElement(businessValueControl).sendKeys(businessValue);
    }

    public void assignedUserDD() {

        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.cssSelector("input[type=\"checkbox\"]")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
    }

   /* public void isBillable() {

        driver.findElement(isBillable).click();
    }*/

    public void TypeDD() {

        WebElement branch = driver.findElement(By.xpath(".//*[@id='ProjectType']"));
        Select type_dd = new Select(branch);
        type_dd.selectByVisibleText("Internal Fixed");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();

    }
    public void clickEditMenu() {
        driver.findElement(editMenu).click();
    }



    public void clickOnDeleteButton(){

        driver.findElement(delete).click();

       /* WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();
        */

    }

    public void clickDeleteMenu() {
        driver.findElement(deleteMenu).click();
    }
    public void cofirmDelete() {
        driver.findElement(confirmDeletion).click();
    }


}