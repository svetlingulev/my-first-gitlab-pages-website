package ui.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ddzhonova on 28.10.16.
 */
public class UserPage {


    WebDriver driver;

    By firstName = By.id("Name");
    By lastName = By.id("LastName");

    By UserButton = By.xpath("//span[@class='title'][contains(text(),'Users')]");

    By SeacrhButton = By.xpath(".//*[@id='formItems']/div[1]/div/div/div/div[2]/div[3]/div/button[1]");
    By AddUserButton = By.xpath("//span[text()='Add new']");
    By Password = By.xpath(".//*[@id='Password']");
    By RepeatPassword = By.xpath(".//*[@id='RepeatPassword']");
    By Username = By.xpath(".//*[@id='username']");

    By Fname = By.xpath(".//*[@id='firstname']");
    By LName = By.xpath(".//*[@id='lastname']");
    By FullName = By.xpath(".//*[@id='NativeFullName']");

    By Phone = By.id("phone");
    By Email = By.id("email");
    By BusinessInfo = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/ul/li[2]/a");

    By DeleteButton = By.xpath(".//*[@id='user-list']/tbody/tr/td[8]/div/div[4]/a/span");
    By EditButton = By.xpath(".//*[@id='user-list']/tbody/tr/td[8]/div/div[3]/a/span");
    By Classification = By.xpath("//input[@class='select2-search__field']");
    By clickClassification = By.xpath(".//*[@id='select2-occupationalClassification-container']");
    By chooseClassification = By.xpath("//span[text()='Вицепрезидент на Република България (1111 9002)']");



    public UserPage(WebDriver driver){
        this.driver=driver;

    }

    public void clickOnUserButton(){

        driver.findElement(UserButton).click();

    }

    public void typeFirstName(String name){

        driver.findElement(firstName).sendKeys(name);
    }

    public void typeLastName(String last){
        driver.findElement(lastName).sendKeys(last);

    }

    public void selectDDValues(){

        WebElement City = driver.findElement(By.id("CityId"));
        Select city_dd = new Select(City);
        city_dd.selectByVisibleText("Sofia");
        Assert.assertEquals(city_dd.getFirstSelectedOption().getText(),"Sofia");

        WebElement Department = driver.findElement(By.id("DepartmentId"));
        Select department_dd = new Select(Department);
        department_dd.selectByVisibleText("Engineering");
        Assert.assertEquals(department_dd.getFirstSelectedOption().getText(),"Engineering");

        WebElement Team = driver.findElement(By.id("TeamId"));
        Select team_dd = new Select(Team);
        team_dd.selectByVisibleText("Inferno");

        WebElement Project = driver.findElement(By.id("ProjectId"));
        Select project_dd = new Select(Project);
        project_dd.selectByVisibleText("Inferno");

        WebElement Position = driver.findElement(By.id("PositionId"));
        Select position_dd = new Select(Position);
        position_dd.selectByVisibleText("QA");

        WebElement CurrentStatus = driver.findElement(By.id("CurrentStatus"));
        Select status_dd = new Select(CurrentStatus);
        status_dd.selectByVisibleText("Active");

        WebElement SortBy = driver.findElement(By.id("UsersSortingType"));
        Select sort_dd = new Select(SortBy);
        sort_dd.selectByIndex(1);

//        WebElement AuthenticationType = driver.findElement(By.id("AuthType"));
//        Select auth_dd = new Select(AuthenticationType);
//        auth_dd.selectByIndex(1);

    }

    public void ClickonSearchButton(){
        driver.findElement(SeacrhButton).sendKeys(Keys.ENTER);

//        WebElement myText = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[3]/div"));
//        String text = myText.getText();
//        Assert.assertTrue("No users".contains(text));

    }

    public void ClickOnAddUsersButton(){
        driver.findElement(AddUserButton).click();

    }

    public void addUserDDvalues(){

        WebElement AuthenticationType = driver.findElement(By.id("AuthType"));
        Select auth_dd = new Select(AuthenticationType);
        auth_dd.selectByVisibleText("HRAuthentication");
    }

    public void typePassword(String pass){

        driver.findElement(Password).sendKeys(pass);
    }

    public void typeRepeatPassword(String rpass){

        driver.findElement(RepeatPassword).sendKeys(rpass);
    }

    public void typeUsername(String user){

        driver.findElement(Username).sendKeys(user);
    }

    public void typeFName(String fname){

        driver.findElement(Fname).sendKeys(fname);
    }

    public void typeLName(String lname){

        driver.findElement(LName).sendKeys(lname);
    }

    public void typeFullName(String fullname){

        driver.findElement(FullName).sendKeys(fullname);
    }

    public void addUserDetailsDD(){

        WebElement PositionType = driver.findElement(By.xpath(".//*[@id='PositionID']"));
        Select position_dd = new Select(PositionType);
        position_dd.selectByVisibleText("QA");

        WebElement Country = driver.findElement(By.xpath(".//*[@id='CountryID']"));
        Select coutry_dd = new Select(Country);
        coutry_dd.selectByVisibleText("Bulgaria");

        WebElement City = driver.findElement(By.xpath(".//*[@id='ddlCities']"));
        Select city_dd = new Select(City);
        city_dd.selectByIndex(1);


//        driver.findElement(By.xpath(".//*[@id='select2-occupationalClassifications-container']")).click();
//
//        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@id='select2-occupationalClassifications-results']//li"));
//
//        for (int i=0;i<dd_menu.size();i++){
//
//            WebElement element = dd_menu.get(i);
//
//            String innerhtml = element.getAttribute("innerHTML");
//
//            if(innerhtml.contentEquals("Авиобояджия, Код: 7132 2009")){
//                element.click();
//                break;
//            }
//
////            System.out.println("values are ======" + innerhtml);
//
//        }

    }
    public void typePhone(String phone){

        driver.findElement(Phone).sendKeys(phone);
    }
    public void typeEmail(String email){

        driver.findElement(Email).sendKeys(email);
    }

    public void addCalendar() {

        driver.findElement(By.xpath(".//*[@id='birthdayPicker']/span/button")).click();

        driver.findElement(By.xpath("html/body/div[6]/div[1]/table/tbody/tr[3]/td[7]")).click();

    }
    public void clickonBusinessInfoButton(){

        driver.findElement(BusinessInfo).click();

    }
    public void addBusinessInfo() {

        WebElement ExpertiseLevel = driver.findElement(By.xpath(".//*[@id='ExpertiseLevelID']"));
        Select expertise_dd = new Select(ExpertiseLevel);
        expertise_dd.selectByVisibleText("Architect");

        WebElement Company = driver.findElement(By.xpath(".//*[@id='CompaniesDd']"));
        Select company_dd = new Select(Company);
        company_dd.selectByVisibleText("Prime");

        WebElement Department = driver.findElement(By.xpath(".//*[@id='DepartmentID']"));
        Select dep_dd = new Select(Department);
        dep_dd.selectByVisibleText("Engineering");

        driver.findElement(By.xpath(".//*[@id='edit-user-form']/div[2]/input")).click();
    }

    public void ClickOnDeleteButton() throws InterruptedException {

        WebElement element = driver.findElement(By.xpath("//i[@class='fa fa-trash-o']"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
        Thread.sleep(2000);

        WebElement element2 = driver.findElement(By.xpath(".//*[@id='delete-confirm']"));
        if (element2.isDisplayed()) {
            element2.click();
        }


    }

    public void ClickOnEditutton() throws InterruptedException {

        WebElement element = driver.findElement(By.xpath("//i[@class='fa fa-pencil']"));
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();

    }

    public void editUserDetailsDD() {

        WebElement PositionType = driver.findElement(By.xpath(".//*[@id='PositionID']"));
        Select position_dd = new Select(PositionType);
        position_dd.selectByVisibleText("HR");

        WebElement Country = driver.findElement(By.xpath(".//*[@id='CountryID']"));
        Select coutry_dd = new Select(Country);
        coutry_dd.selectByVisibleText("England");

        WebElement City = driver.findElement(By.xpath(".//*[@id='ddlCities']"));
        Select city_dd = new Select(City);
        city_dd.selectByIndex(1);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void editPhone(){

        driver.findElement(Phone).sendKeys(Keys.chord(Keys.CONTROL, "a"), "35989909090");
    }
    public void editEmail(){

        driver.findElement(Email).sendKeys(Keys.chord(Keys.CONTROL, "a"), "sf@gmail.com");
    }

    public void editBusinessInfo() throws InterruptedException {

        WebElement ExpertiseLevel = driver.findElement(By.xpath(".//*[@id='ExpertiseLevelID']"));
        Select expertise_dd = new Select(ExpertiseLevel);
        expertise_dd.selectByVisibleText("QA");

        Thread.sleep(1000);

        WebElement Company = driver.findElement(By.xpath(".//*[@id='CompaniesDd']"));
        Select company_dd = new Select(Company);
        company_dd.selectByVisibleText("Prime");

        WebElement Department = driver.findElement(By.xpath(".//*[@id='DepartmentID']"));
        Select dep_dd = new Select(Department);
        dep_dd.selectByVisibleText("QA");

        driver.findElement(By.xpath(".//*[@id='edit-user-form']/div[2]/input")).click();
    }

    public void clickOnClassification(){
        driver.findElement(clickClassification).click();

    }

    public void typeClassification(String occupationalClassification){

        driver.findElement(Classification).sendKeys(occupationalClassification);
    }

    public void chooseClassification(){
        driver.findElement(chooseClassification).click();

    }



    }
