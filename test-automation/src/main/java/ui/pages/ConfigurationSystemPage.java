package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ConfigurationSystemPage {

    WebDriver driver;

    By configuration = By.xpath("html/body/div[4]/div[1]/div/div/ul/li[10]/a/span[1]");
    By system = By.xpath(".//*[@id='administration']/li[1]/a/span");
    By emailHost = By.xpath(".//*[@id='EmailHost']");
    By port = By.xpath(".//*[@id='EmailPort']");
    By emailFrom = By.xpath(".//*[@id='EmailSender']");
    By smtpPassword = By.xpath(".//*[@id='SMTPPassword']");
    By smtpUsername = By.xpath(".//*[@id='SMTPUsername']");
    By adminPassword = By.xpath(".//*[@id='HRadministratorPass']");
    By adminEmail = By.xpath(".//*[@id='ErrorAdministratorEmail']");
    By errorAdminEmail = By.xpath(".//*[@id='ErrorAdministratorEmail']");
    By emailDaysOff = By.xpath(".//*[@id='AnnualDaysOffAdministratorEmail']");
    By ldap = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[1]/ul/li[2]/a");
    By ldapHost = By.xpath(".//*[@id='LDAPHostName']");
    By ldapIP = By.xpath(".//*[@id='LDAPIP']");
    By ldapPort = By.xpath(".//*[@id='LDAPPortNumber']");
    By jira = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[1]/ul/li[3]/a");
    By jiraPassword = By.xpath(".//*[@id='JiraPassword']");
    By devStats = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[1]/ul/li[4]/a");
    By devStatsPassword = By.xpath(".//*[@id='DevStatsPassword']");
    By reminders = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[1]/ul/li[5]/a");
    By emailNewAccounts = By.xpath(".//*[@id='NewAcountCreatedAdministrator']");
    By emailDueDates = By.xpath(".//*[@id='DueDateAdministrator']");
    By save = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[2]/div[2]/input");
    By testEmail = By.xpath(".//*[@id='EmailTo']");
    By administration = By.xpath(".//*[@id='edit-user-form']/div/div/div/div[1]/ul/li[1]/a");
    By send = By.xpath(".//*[@id='testEmail']");
    By testMail = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[1]/span");
    By eventLogs = By.xpath("//span[@class='title'][contains(text(),'Event Logs')]");
    By message = By.xpath(".//*[@id='Message']");
    By search = By.xpath(".//*[@id='search']");


    public ConfigurationSystemPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickConfiguration() {
        driver.findElement(configuration).click();
    }

    public void clickSystem() {
        driver.findElement(system).click();
    }

    public void typeEmailHost() {
        driver.findElement(emailHost).sendKeys(Keys.chord(Keys.CONTROL, "a"), "mailtrap.io");
    }

    public void typeEmailPort() {
        driver.findElement(port).sendKeys(Keys.chord(Keys.CONTROL, "a"), "465");
    }

    public void typeEmailFrom() {
        driver.findElement(emailFrom).sendKeys(Keys.chord(Keys.CONTROL, "a"), "postmaster@mg.programista.pro");
    }

    public void typeSMTPUsername() {
        driver.findElement(smtpUsername).sendKeys(Keys.chord(Keys.CONTROL, "a"), "ab000d12fa4465");
    }

    public void typeSMTPPassword() {
        driver.findElement(smtpPassword).sendKeys(Keys.chord(Keys.CONTROL, "a"), "4a3d825e5c7584");
    }

    public void typeAdminPassword() {
        driver.findElement(adminPassword).sendKeys(Keys.chord(Keys.CONTROL, "a"), "pass");
    }

    public void typeAdminEmail() {
        driver.findElement(adminEmail).sendKeys(Keys.chord(Keys.CONTROL, "a"), "fenek114@abv.bg");
    }

    public void typeErrorAdminEmail() {
        driver.findElement(errorAdminEmail).sendKeys(Keys.chord(Keys.CONTROL, "a"), "fenek114@abv.bg");
    }

    public void typeEmailDaysOff() {
        driver.findElement(emailDaysOff).sendKeys(Keys.chord(Keys.CONTROL, "a"), "postmaster@mg.programista.pro");
    }

    public void clickLDAP() {
        driver.findElement(ldap).click();
    }

    public void typeLdapHostName() {
        driver.findElement(ldapHost).sendKeys(Keys.chord(Keys.CONTROL, "a"), "LDAP://10.194.51.71:389/");
    }

    public void typeLdapIP() {
        driver.findElement(ldapIP).sendKeys(Keys.chord(Keys.CONTROL, "a"), "10.194.51.71");
    }

    public void typeLdapPort() {
        driver.findElement(ldapPort).sendKeys(Keys.chord(Keys.CONTROL, "a"), "389");
    }

    public void clickJira() {
        driver.findElement(jira).click();
    }

    public void typeJiraPassword() {
        driver.findElement(jiraPassword).sendKeys(Keys.chord(Keys.CONTROL, "a"), "123");
    }

    public void clickDevStats() {
        driver.findElement(devStats).click();
    }

    public void typeDevStatsPassword() {
        driver.findElement(devStatsPassword).sendKeys(Keys.chord(Keys.CONTROL, "a"), "/\\YL;xtvDu4hTgW`");
    }

    public void clickReminders() {
        driver.findElement(reminders).click();
    }

    public void typeEmailNewAccounts() {
        driver.findElement(emailNewAccounts).sendKeys(Keys.chord(Keys.CONTROL, "a"), "fenek114@abv.bg");
    }

    public void typeEmailDuedates() {
        driver.findElement(emailDueDates).sendKeys(Keys.chord(Keys.CONTROL, "a"), "fenek114@abv.bg");
    }

    public void remindersDD() {

        driver.findElement(By.xpath(".//*[@id='tab-reminders']/div[2]/div[2]/div/span/div/button")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath(".//*[@id='tab-reminders']/div[2]/div[2]/div/span/div/ul/li/a/label/input"));

        for (WebElement el : dd_menu) {
            el.click();
            break;
        }
    }

    public void clickSave() {
        driver.findElement(save).click();
    }

    public void typeTestEmail(String email){
        driver.findElement(testEmail).sendKeys(email);
    }

    public void clickAdministration() {
        driver.findElement(administration).click();
    }

    public void clickSend() {
        driver.findElement(send).click();
    }

    public void clickOnTestMail(){

        driver.findElement(testMail).click();

    }

    public void clickOnEventLogs(){
        driver.findElement(eventLogs).click();
    }

    public void fromDate() {
        driver.findElement(By.cssSelector("button.btn.default")).click();
        driver.findElement(By.cssSelector("td.today.day")).click();
    }

    public void toDate() throws InterruptedException {
        driver.findElement(By.xpath(".//*[@id='datepickerToDate']/span/button")).sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        driver.findElement(By.xpath("//td[@class='today active day']")).click();
    }

    public void eventLogTypeDD() {

        WebElement eventLog = driver.findElement(By.xpath(".//*[@id='EventLogType']"));
        Select event_dd = new Select(eventLog);
        event_dd.selectByVisibleText("Information");
        Assert.assertEquals(event_dd.getFirstSelectedOption().getText(), "Information");
    }

    public void userDD() {

        driver.findElement(By.xpath(".//*[@id='select2-userSelect-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath(".//*[@id='select2-userSelect-results']/li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Admin Admin")) {
                element.click();
                break;
            }
        }
    }

    public void typeMessage(String msg){
        driver.findElement(message).sendKeys(msg);
    }

    public void clickSearch(){
        driver.findElement(search).click();
    }


}