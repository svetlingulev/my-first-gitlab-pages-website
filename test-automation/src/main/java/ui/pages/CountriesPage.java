package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CountriesPage {

    WebDriver driver;

    By country = By.xpath("//span[@class='title'][contains(text(),'Countries')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath("(//a[@class='btn green-meadow btn-outline'])[1]");
    By delete = By.xpath("(//a[@class='btn red btn-outline'])[1]");



    public CountriesPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickCountries() {

        driver.findElement(country).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String country) {

        driver.findElement(name).sendKeys(country);
    }

    public void editName() {

        driver.findElement(name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A test country edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}