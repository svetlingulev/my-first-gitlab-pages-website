package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class PositionsPage {

    WebDriver driver;

    By position = By.xpath("//span[@class='title'][contains(text(),'Positions')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By Name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='dataTables-listBps']/tbody/tr[1]/td[2]/a[1]");
    By delete = By.xpath(".//*[@id='dataTables-listBps']/tbody/tr[1]/td[2]/a[2]");

    public PositionsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickPositions() {

        driver.findElement(position).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void editPosition() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A position test");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}