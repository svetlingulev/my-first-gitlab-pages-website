package ui.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by ddzhonova on 17.11.16.
 */
public class TasksPage {

    WebDriver driver;

    By AddNew = By.xpath("//span[text()='Add new']");
    By Title = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/div/input");
    By Description = By.xpath(".//div[@class='note-editor note-frame panel panel-default']/div[@class='note-editing-area']/div[@class='note-editable panel-body']");
    By Save = By.xpath(".//*[@id='submit']");
    By Title2 = By.xpath(".//*[@id='Title']");
    By Search = By.xpath(".//*[@id='search']");
    By Edit = By.xpath("//i[@class='fa fa-pencil task-action-width']");
    By Submit = By.xpath(".//*[@id='submit']");
    By Tasks = By.xpath("//span[@class='title'][contains(text(),'Tasks')]");
    By Complete = By.xpath("//i[@class='fa fa-check-square-o task-action-width']");
    By Note = By.xpath(".//*[@id='Note']");
    By SubmitNote = By.xpath("//input[@class='btn green btn-outline']");
    By Reopen = By.xpath("//i[@class='fa fa-play task-action-width']");
    By Comments = By.xpath("//span[@class='badge badge-default']");
    By WriteComments = By.xpath(".//*[@id='commentText']");
    By SubmitComments = By.xpath("//i[@class='fa fa-commenting-o icon-white']");
    By StartAction = By.xpath("(//table[@id='table-tasks']/tbody/tr/td[11]/a/i)[1]");
    By Reject = By.xpath("(//i[@class='fa fa-ban task-action-width'])[1]");
    By ToDo = By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[11]/a[1]/i");
    By Review = By.xpath("(//i[@class='material-icons'])[2]");
    By demo = By.xpath(".//*[@id='main']/div/div[2]/div/div/div[2]/table/tbody/tr/td[1]/div[1]/strong/a/span");
    By deletedMail = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[1]");
    By editedMail = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[2]");
    By assignedTaskMail = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[3]");
    By assignedTaskMail2 = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[4]");
    By editedMail2 = By.xpath(".//*[@id='main']/div/div[1]/div/ul/li[3]");
    By comment = By.xpath(".//*[@id='Comment']");



    public TasksPage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickAddNewButton() {

        driver.findElement(AddNew).click();
    }

    public void clickTasks() {

        driver.findElement(Tasks).click();

    }
    public void typeTitle(String title) {

        driver.findElement(Title).sendKeys(title);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    public void selectAssignToDepartment() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/div/span/span[1]/span")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='select2-results__options']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("QA")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void selectAssignToQADepartment() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/div/span/span[1]/span")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='select2-results__options']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("QA")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void selectAssignToUser() {

        driver.findElement(By.id("select2-assignedToUser-container")).click();

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@id='select2-assignedToUser-results']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("Admin Admin")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void selectAssignToDifferentUser() {

        driver.findElement(By.id("select2-assignedToUser-container")).click();

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@id='select2-assignedToUser-results']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("Kris Fernandez")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void dueDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerDueDate']/span/button")).click();
//        driver.findElement(By.xpath(" //td[@class='today day']")).click();
        List<WebElement> dates = driver.findElements(By.xpath("//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-bottom']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for(int i=0;i<total_node;i++){
            String date=dates.get(i).getText();
            if(date.equalsIgnoreCase("22")){
                dates.get(i).click();
                break;
            }

        }

    }

    public void typeDescription (String description) {

//        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/div/div/div[2]/div[1]/button[1]")).sendKeys(Keys.ENTER);
        driver.findElement(Description).sendKeys(description);

    }
    public void addPriorityDD() {

        WebElement Priority = driver.findElement(By.xpath(".//*[@id='Priority']"));
        Select priority_dd = new Select(Priority);
        priority_dd.selectByVisibleText("High");
    }

    public void clickSaveButton() {

        driver.findElement(Save).click();

    }

//    public void main() {
//        int day, month, year;
//        GregorianCalendar date = new GregorianCalendar();
//        day = date.get(Calendar.DAY_OF_MONTH);
//        month = date.get(Calendar.MONTH)+1;
//        year = date.get(Calendar.YEAR);
//        String today = "a_"+year+"_"+month+"_"+day;
//        System.out.println(today);
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        driver.findElement(By.id("datepickerStartBtn")).click();
//        driver.findElement(By.xpath("//a[@id='\"+today+\"']")).click();
//
//    }

    public void TypeTitle2(String title) {

        driver.findElement(Title2).sendKeys(title);

    }

    public void clickSearchButton() {

        driver.findElement(Search).sendKeys(Keys.ENTER);

    }

    public void fromDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerStartBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//td[@class='today day']")).click();
    }

    public void toDate() {
        driver.findElement(By.xpath(".//*[@id='datepickerEndBtn']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//td[@class='today day']")).click();
    }

    public void priorityDD() {

        WebElement Priority = driver.findElement(By.xpath(".//*[@id='TaskStatus']"));
        Select priority_dd = new Select(Priority);
        priority_dd.selectByVisibleText("All");
    }

    public void creatorDD() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/form/div[1]/div/div/div/div[2]/div[2]/div[2]/span/span[1]/span")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='select2-results__options']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("All")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void assigneeDD() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/form/div[1]/div/div/div/div[2]/div[2]/div[3]/span/span[1]/span")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='select2-results__options']//li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("All")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void clickEditButton() {

        driver.findElement(By.linkText("Actions")).click();
        driver.findElement(By.linkText("Edit")).click();

    }

    public void changeStatusDD() {

        WebElement Status = driver.findElement(By.xpath(".//*[@id='CurrentStatus']"));
        Select status_dd = new Select(Status);
        status_dd.selectByVisibleText("Done");
    }

    public void clickSubmitButton() {

        driver.findElement(Submit).sendKeys(Keys.ENTER);

    }

    public void clickOnDeleteButton() throws InterruptedException {

        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(500);
        driver.findElement(By.linkText("Delete")).click();
        Thread.sleep(500);
        driver.findElement(By.id("delete-confirm")).click();
    }

    public void editTitle() {

        driver.findElement(Title).sendKeys(Keys.chord(Keys.CONTROL, "a"), "editedTitle");
    }

    public void statusDD() {

        WebElement Priority = driver.findElement(By.xpath(".//*[@id='CurrentStatus']"));
        Select priority_dd = new Select(Priority);
        priority_dd.selectByVisibleText("All");
    }

    public void selectDepartment() {

        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/form/div[1]/div/div/div/div[2]/div[2]/div[4]/span/span[1]/span")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@id='select2-departmentSelect-results']/li"));

        for (int i=0;i<dd_menu.size();i++){

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if(innerhtml.contentEquals("All")){
                element.click();
                break;
            }

//            System.out.println("values are ======" + innerhtml);

        }
    }

    public void addVisibleTasks(){

        driver.findElement(By.xpath("//button[@class='multiselect dropdown-toggle btn btn-default']")).click();

        List<WebElement> dd_menu=driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu) {
            el.click();
            break;

        }
        List<WebElement> dd_menu2=driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu2) {
            el.click();
            break;
        }
//        driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/form/div[1]/div/div/div/div[2]/div[3]/div/span/div/button")).click();

    }

    public void clickCompleteButton() throws InterruptedException {

        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(1000);
        driver.findElement(By.linkText("Complete")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();

    }

    public void Note (String note){

        driver.findElement(Note).sendKeys(note);

    }

    public void clickSubmitNoteButton() {

        driver.findElement(SubmitNote).click();

    }
    public void clickReopenButton() throws InterruptedException {

        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(1000);
        driver.findElement(By.linkText("Reopen")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();

    }

    public void clickCommentsButton() {

        driver.findElement(Comments).click();

    }

    public void Comments (String comments){

        driver.findElement(WriteComments).sendKeys(comments);

    }

    public void clickSubmitComments() {

        driver.findElement(SubmitComments).click();

    }

    public void clickStartAction() {

        driver.findElement(StartAction).click();

    }

    public void clickReject() throws InterruptedException {
        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(500);
        driver.findElement(By.linkText("Reject")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();
    }

    public void clickToDo() throws InterruptedException {
        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(500);
        driver.findElement(By.linkText("assignmentSet as To Do")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();
    }

    public void clickReview() throws InterruptedException {
        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(500);
        driver.findElement(By.linkText("rate_reviewSet In Review")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();
    }

    public void clickOnAdminButton() {

        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/ul/li/a/span"));
        action.moveToElement(we).moveToElement(driver.findElement(By.xpath("html/body/div[2]/div/div[2]/ul/li/ul/li[2]/a"))).click().build().perform();
    }

    public void clickDemoInbox(){

        driver.findElement(demo).click();

    }

    public void clickOnTaskDeletedMail(){

        driver.findElement(deletedMail).click();

    }

    public void clickOnTaskEditedMail(){

        driver.findElement(editedMail).click();
    }

    public void clickOnAssignedTask(){
        driver.findElement(assignedTaskMail).click();
    }

    public void clickOnAssignedTask2(){
        driver.findElement(assignedTaskMail2).click();
    }

    public void clickOnTaskEditedMail2(){

        driver.findElement(editedMail2).click();
    }

    public void typeComment (String writeComment) {

        driver.findElement(comment).sendKeys(writeComment);

    }
    public void startButton() throws InterruptedException {

        driver.findElement(By.linkText("Actions")).click();
        Thread.sleep(500);
        driver.findElement(By.linkText("Start")).click();
        Thread.sleep(500);
        driver.findElement(By.id("Note")).clear();
        driver.findElement(By.id("Note")).sendKeys("test");
        driver.findElement(By.xpath("//input[@value='Submit']")).click();
    }


 }
