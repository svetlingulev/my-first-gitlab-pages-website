package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ExpertiseLevelsPage {

    WebDriver driver;

    By experiseLevels = By.xpath("//span[@class='title'][contains(text(),'Expertise Levels')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By name = By.id("Name");
    By fee = By.xpath(".//*[@id='Fee']");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Expertise levels']/tbody/tr[1]/td[3]/a[1]");
    By delete = By.xpath(".//*[@id='Expertise levels']/tbody/tr[1]/td[3]/a[2]");



    public ExpertiseLevelsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickExpertiseLevel() {

        driver.findElement(experiseLevels).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String holiday) {

        driver.findElement(name).sendKeys(holiday);
    }

    public void typeFee(String holiday) {

        driver.findElement(fee).sendKeys(holiday);
    }

    public void editName() {

        driver.findElement(name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "An expertise level edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}