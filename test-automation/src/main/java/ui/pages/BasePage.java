package ui.pages;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class BasePage {
    public WebDriver driver;
    public WebDriverWait wait;
    public Logger logger = Logger.getLogger(BasePage.class);

    //Constructor
    public BasePage (WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.wait = wait;
        File log4jConfig = new File("src/main/resources/log4j.properties");

        //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure(new FileInputStream(log4jConfig));
        logger.debug("======================================================================");
    }

    //Click Method
    public void click (By elementLocation) {
        driver.findElement(elementLocation).click();
    }

    //Write Text
    public void writeText (By elementLocation, String text) {
        driver.findElement(elementLocation).sendKeys(text);
    }

    //Read Text
    public String readText (By elementLocation) {
        return driver.findElement(elementLocation).getText();
    }
}