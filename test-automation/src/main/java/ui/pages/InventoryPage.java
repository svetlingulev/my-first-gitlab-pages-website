package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class InventoryPage {

    WebDriver driver;

    By addNew = By.xpath("//span[text()='Add new']");
    By typeName = By.xpath(".//*[@id='Type']");
    By typeYears = By.xpath(".//*[@id='Years']");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By inventory = By.xpath("//span[@class='title'][contains(text(),'Inventory')]");
    By itemTypes = By.xpath(".//*[@id='inventory']/li[1]/a/span");
    By page = By.xpath("(//a[@href='/ItemTypes?Pager.CurrentPage=2'])[1]");
    By edit = By.xpath(".//*[@id='ItemTypes']/table/tbody/tr/td[5]/a[1]");
    By listType = By.xpath(".//*[@id='type']");
    By search = By.xpath(".//*[@id='search']");
    By delete = By.xpath("//a[@class='btn red btn-outline']");
    By deleteConfirm = By.xpath(".//*[@id='delete-confirm']");
    By items = By.xpath(".//*[@id='inventory']/li[2]/a/span");
    By inventoryNumber = By.xpath(".//*[@id='InventoryNumber']");
    By typeModel = By.xpath(".//*[@id='Model']");
    By typeSupplier = By.xpath(".//*[@id='Supplier']");
    By typePrice = By.xpath(".//*[@id='Price']");
    By typeComment = By.xpath(".//*[@id='Description']");
    By  listInventoryNumber = By.xpath(".//*[@id='i']");
    By listModel = By.xpath(".//*[@id='mo']");
    By serialNumber = By.xpath(".//*[@id='Context']");
    By listSerialNumber = By.xpath(".//*[@id='sn']");
    By details = By.xpath("//i[@class='fa fa-list-alt']");
    By close = By.xpath(".//*[@id='modal-details']/div/div[2]/div[3]/button");
    By itemsEdit = By.xpath(".//*[@id='Items']/table/tbody/tr/td[10]/a[2]");
    By editedModel = By.xpath(".//*[@id='Model']");
    By itemDelete = By.xpath(".//*[@id='Items']/table/tbody/tr/td[10]/a[3]");
    By request = By.xpath(".//*[@id='inventory']/li[4]/a/span");
    By requestItem = By.xpath("//span[text()='Request an item']");
    By descriptionType = By.xpath(".//*[@id='Description']");
    By itemRequets = By.xpath("html/body/div[4]/div[2]/div/div/div/div/form/div/div[3]/input");
    By userRequets = By.xpath(".//*[@id='inventory']/li[5]/a/span");
    By approve = By.xpath("//span[text()='Approve']");
    By executiveGroup = By.xpath(".//*[@id='inventory']/li[6]/a/span");
    By take = By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[5]/a[1]");
    By deliver = By.xpath("//span[text()='Deliver']");
    By receive = By.xpath("//span[text()='Recieve']");


    public InventoryPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickOnAddNew(){

        driver.findElement(addNew).click();
    }

    public void executiveGroupDD() {

        WebElement group = driver.findElement(By.xpath(".//*[@id='ExecutiveGroup_ID']"));
        Select group_dd = new Select(group);
        group_dd.selectByVisibleText("paper");
        Assert.assertEquals(group_dd.getFirstSelectedOption().getText(), "paper");
    }

    public String generateRandomString(int length){
        return RandomStringUtils.randomAlphabetic(length);
    }

    public void typeName(String name){

        driver.findElement(typeName).sendKeys(name + generateRandomString(10));
    }

    public void listType(String type){

        driver.findElement(listType).sendKeys(type);
    }

    public void typeYears(String years){

        driver.findElement(typeYears).sendKeys(years);
    }


    public void clickSave(){

        driver.findElement(save).click();
    }

    public void clickInventory(){

        driver.findElement(inventory).click();
    }

    public void clickItemTypes(){

        driver.findElement(itemTypes).click();
    }

    public void clickPage(){

        driver.findElement(page).click();
    }

    public void clickEdit(){

        driver.findElement(edit).click();
    }

    public void typeEditedInventory() {

        driver.findElement(typeName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "edittete");
    }

    public void selectGroupDD() {

        WebElement group = driver.findElement(By.xpath(".//*[@id='exe']"));
        Select group_dd = new Select(group);
        group_dd.selectByVisibleText("paper");
        Assert.assertEquals(group_dd.getFirstSelectedOption().getText(), "paper");
    }

    public void approval() {

        WebElement approval = driver.findElement(By.xpath(".//*[@id='RequiresManagerApproval']"));
        Select approval_dd = new Select(approval);
        approval_dd.selectByVisibleText("Yes");
        Assert.assertEquals(approval_dd.getFirstSelectedOption().getText(), "Yes");
    }


    public void clickSearch(){

        driver.findElement(search).click();
    }

    public void clickOnDelete() throws InterruptedException {
        driver.findElement(delete).click();
        Thread.sleep(1000);
        driver.findElement(deleteConfirm).click();
    }

    public void clickItems(){

        driver.findElement(items).click();
    }

    public void itemTypeDD() {

        driver.findElement(By.xpath(".//*[@id='select2-itemTypeSelect-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@id='select2-itemTypeSelect-results']//li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("copy paper")) {
                element.click();
                break;
            }
        }
    }

    public void typeInventoryNumber(String number){

        driver.findElement(inventoryNumber).sendKeys(number);
    }

    public void listInventoryNumber(String number){

        driver.findElement(listInventoryNumber).sendKeys(number);
    }

    public void typeModel(String model){

        driver.findElement(typeModel).sendKeys(model);
    }

    public void editedModel() {
        driver.findElement(editedModel).sendKeys(Keys.chord(Keys.CONTROL, "a"), "edited name");
    }

    public void listModel(String model){

        driver.findElement(listModel).sendKeys(model);
    }

    public void countryDD() {

        WebElement country = driver.findElement(By.xpath(".//*[@id='country-dropdown']"));
        Select country_dd = new Select(country);
        country_dd.selectByVisibleText("Bulgaria");
        Assert.assertEquals(country_dd.getFirstSelectedOption().getText(), "Bulgaria");
    }
    public void listCountryDD() {

        WebElement country = driver.findElement(By.xpath(".//*[@id='co']"));
        Select country_dd = new Select(country);
        country_dd.selectByVisibleText("Bulgaria");
        Assert.assertEquals(country_dd.getFirstSelectedOption().getText(), "Bulgaria");
    }


    public void cityDD() {

        WebElement city = driver.findElement(By.xpath(".//*[@id='ddlCities']"));
        Select city_dd = new Select(city);
        city_dd.selectByIndex(1);
    }

    public void locationDD() {

        WebElement location = driver.findElement(By.xpath(".//*[@id='ddlLocations']"));
        Select location_dd = new Select(location);
        location_dd.selectByIndex(1);
    }

    public void locationUnitDD() {

        WebElement locationUnit = driver.findElement(By.xpath(".//*[@id='ddlLocationUnits']"));
        Select locunit_dd = new Select(locationUnit);
        locunit_dd.selectByIndex(1);
    }

    public void typeSupplier (String supplier){

        driver.findElement(typeSupplier).sendKeys(supplier);
    }

    public void typePrice (String price){

        driver.findElement(typePrice).sendKeys(price);
    }

//    public void buyDate() {
//        driver.findElement(By.xpath(".//*[@id='BuyDate']")).sendKeys(Keys.ENTER);
//        driver.findElement(By.xpath(" //td[@class='today day']")).click();
//
//    }

    public void buyDate() {

        driver.findElement(By.xpath(".//*[@id='BuyDate']")).sendKeys(Keys.ENTER);

        List<WebElement> dates = driver.findElements(By.xpath(".//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

        int total_node = dates.size();

        for (int i = 0; i < total_node; i++) {
            String date = dates.get(i).getText();
            if (date.equalsIgnoreCase("1")) {
                dates.get(i).click();
                break;
            }

        }
    }

    public void typeComment (String comment){

        driver.findElement(typeComment).sendKeys(comment);
    }

    public void addComponents() {

        driver.findElement(By.xpath("//button[@class='multiselect dropdown-toggle btn btn-default']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("(//ul[@class='multiselect-container dropdown-menu']//li/a/label/input)"));

        for (WebElement el : dd_menu) {
            el.click();
            break;

        }
    }

    public void typeSerialNumber (String serial){

        driver.findElement(serialNumber).sendKeys(serial);
    }

    public void listSerialNumber (String serial){

        driver.findElement(listSerialNumber).sendKeys(serial);
    }


    public void assignedUserDD() {

        WebElement user = driver.findElement(By.xpath(".//*[@id='UserID']"));
        Select user_dd = new Select(user);
        user_dd.selectByVisibleText("Admin Admin");
        Assert.assertEquals(user_dd.getFirstSelectedOption().getText(), "Admin Admin");
    }

    public void listAssignedUserDD() {

        driver.findElement(By.xpath(".//*[@id='select2-UserID-container']")).click();

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@id='select2-UserID-results']//li"));

        for (int i = 0; i < dd_menu.size(); i++) {

            WebElement element = dd_menu.get(i);

            String innerhtml = element.getAttribute("innerHTML");

            if (innerhtml.contentEquals("Admin Admin")) {
                element.click();
                break;
            }
        }
    }

    public void clickDetails(){

        driver.findElement(details).click();
    }

    public void clickClose(){

        driver.findElement(close).click();
    }

    public void clickItemsEdit() {
        driver.findElement(itemsEdit).click();
    }

    public void clickItemsDelete() throws InterruptedException {
        driver.findElement(itemDelete).click();
        Thread.sleep(1000);
        driver.findElement(deleteConfirm).click();
    }

    public void clickMyItemsRequestst(){

        driver.findElement(request).click();
    }

    public void requestAnItem(){

        driver.findElement(requestItem).click();
    }

    public void addType() {

        WebElement addType = driver.findElement(By.xpath(".//*[@id='ItemTypeID']"));
        Select addType_dd = new Select(addType);
        addType_dd.selectByVisibleText("testq");
        Assert.assertEquals(addType_dd.getFirstSelectedOption().getText(), "testq");
    }

    public void typeDescription (String descriptioin){

        driver.findElement(descriptionType).sendKeys(descriptioin);
    }

    public void request(){

        driver.findElement(itemRequets).click();
    }
    public void userItemRequest(){

        driver.findElement(userRequets).click();
    }

    public void approveRequets(){

        driver.findElement(approve).click();
    }

    public void executiveGroupRequests(){

        driver.findElement(executiveGroup).click();
    }

    public void clickTake(){

        driver.findElement(take).click();
    }

    public void clickDeliver(){

        driver.findElement(deliver).click();
    }

    public void clickReceive(){

        driver.findElement(receive).click();
    }

  }