package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ExecutiveGroupPage {

    WebDriver driver;

    By executiveGroup = By.xpath("//span[@class='title'][contains(text(),'Executive Groups')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By Name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Clients']/tbody/tr[1]/td[3]/a[1]");
    By delete = By.xpath(".//*[@id='Clients']/tbody/tr[1]/td[3]/a[2]");

    public ExecutiveGroupPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickExecutiveGroup() {

        driver.findElement(executiveGroup).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void editExecutiveGroup() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "An executive group edited");
    }

    public void addExecutiveGroup() {

        driver.findElement(By.xpath("//button[@class='multiselect dropdown-toggle btn btn-default']")).sendKeys(Keys.ENTER);

        List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']//li/a/label/input"));

        for (WebElement el:dd_menu) {
            el.click();
            break;
        }
        driver.findElement(By.xpath(".//*[@id='Name']")).click();
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}