package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ClientsPage {

    WebDriver driver;

    By clients = By.xpath("//span[@class='title'][contains(text(),'Clients')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By clientsName = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Clients']/tbody/tr[4]/td[2]/a[1]");
    By delete = By.xpath(".//*[@id='Clients']/tbody/tr[4]/td[2]/a[2]");



    public ClientsPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickClients() {

        driver.findElement(clients).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeClientsName(String holiday) {

        driver.findElement(clientsName).sendKeys(holiday);
    }

    public void editedClientsName() {

        driver.findElement(clientsName).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A clients name - edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}