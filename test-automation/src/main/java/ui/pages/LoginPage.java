package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class LoginPage extends BasePage{

    //*********Constructor*********
    public LoginPage(WebDriver driver, WebDriverWait wait) throws IOException {
        super(driver, wait);
    }

    //*********Web Elements*********
    String usernameId = "Username";
    String passwordId = "Password";
    String loginButtonClass = "btn";//use class as we dont have id


    //*********Page Methods*********

    public void loginToInferno (String username, String password){
        logger.debug("=========================Login Page================================");
        //Enter Username(Email)
        writeText(By.id(usernameId),username);
        //Enter Password
        writeText(By.id(passwordId), password);
        //Click Login Button
        click(By.className(loginButtonClass));
    }

    //Verify Username Condition
    public void verifyLoginUserName (String expectedText) {
       // Assert.assertEquals(readText(By.xpath(errorMessageUsernameXpath)), expectedText);
    }

    //Verify Password Condition
    public void verifyLoginPassword (String expectedText) {
       // Assert.assertEquals(readText(By.xpath(errorMessagePasswordXpath)), expectedText);
    }

}
