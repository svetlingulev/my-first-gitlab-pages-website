package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class BranchesPage {

    WebDriver driver;

    By branch = By.xpath("//span[@class='title'][contains(text(),'Branches')]");
    By addNew = By.xpath("//a[@class='btn blue btn-outline']");
    By Name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='dataTables-listBps']/tbody/tr[1]/td[2]/a[1]");
    By delete = By.xpath(".//*[@id='dataTables-listBps']/tbody/tr[1]/td[2]/a[2]");

    public BranchesPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickBranches() {

        driver.findElement(branch).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void editName() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A branch - edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }
}