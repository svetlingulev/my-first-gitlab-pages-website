package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CompaniesPage {

    WebDriver driver;

    By company = By.xpath("//span[@class='title'][contains(text(),'Companies')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By Name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath(".//*[@id='Clients']/tbody/tr[1]/td[5]/a[1]");
    By delete = By.xpath(".//*[@id='Clients']/tbody/tr[1]/td[5]/a[2]");
    By jiraProjectsName = By.xpath(".//*[@id='JiraProjectName']");
    By isBillable = By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/form/div[2]/div/div/div[6]/div/span/input");
    By City = By.id("NativeCityName");
    By address = By.xpath(".//*[@id='Address']");
    By owner = By.xpath(".//*[@id='Owner']");
    By eik = By.xpath(".//*[@id='EIK']");



    public CompaniesPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickCompanies() {

        driver.findElement(company).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void typeName(String name) {

        driver.findElement(Name).sendKeys(name);
    }

    public void typeCity(String city) {

        driver.findElement(City).sendKeys(city);
    }

    public void typeAddress(String companyAddress) {

        driver.findElement(address).sendKeys(companyAddress);
    }

    public void typeOwner(String companyOwner) {

        driver.findElement(owner).sendKeys(companyOwner);
    }

    public void typeEIK(String companyEIK) {

        driver.findElement(eik).sendKeys(companyEIK);
    }

    public void editName() {

        driver.findElement(Name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1st company test - edited");
    }

    public void parentCompanyDD() {

        WebElement parentComapny = driver.findElement(By.xpath(".//*[@id='ParentCompanyID']"));
        Select parentCompany_dd = new Select(parentComapny);
        parentCompany_dd.selectByVisibleText("Prime");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}