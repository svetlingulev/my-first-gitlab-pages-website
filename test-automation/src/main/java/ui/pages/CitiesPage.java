package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CitiesPage {

    WebDriver driver;

    By city = By.xpath("//span[@class='title'][contains(text(),'Cities')]");
    By addNew = By.xpath("//span[text()='Add new']");
    By name = By.id("Name");
    By save = By.xpath("//input[@class='btn green btn-outline']");
    By edit = By.xpath("(//a[@class='btn green-meadow btn-outline'])[1]");
    By delete = By.xpath("(//a[@class='btn red btn-outline'])[1]");
    By zip = By.xpath(".//*[@id='ZIP']");



    public CitiesPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clickCities() {

        driver.findElement(city).click();
    }

    public void clickAddNew() {

        driver.findElement(addNew).click();
    }

    public void addCountry() {

        WebElement country = driver.findElement(By.xpath(".//*[@id='countryList']"));
        Select country_dd = new Select(country);
        country_dd.selectByVisibleText("Bulgaria");
    }

    public void typeName(String country) {

        driver.findElement(name).sendKeys(country);
    }

    public void typeZIP(String country) {

        driver.findElement(zip).sendKeys(country);
    }

    public void editName() {

        driver.findElement(name).sendKeys(Keys.chord(Keys.CONTROL, "a"), "A test city edited");
    }

    public void clickSave() {

        driver.findElement(save).click();
    }

    public void clickEdit() {

        driver.findElement(edit).click();
    }

    public void clickOnDeleteButton(){

        driver.findElement(delete).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='delete-confirm']"))).click();

    }


}