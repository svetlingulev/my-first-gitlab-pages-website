package ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class HomePage extends BasePage {

    //*********Constructor*********
    public HomePage (WebDriver driver, WebDriverWait wait) throws IOException {
        super(driver, wait);
    }

    //*********Page Variables*********
    String baseURL = "http://10.194.51.165:8080";


    //*********Page Methods*********

    //Go to Homepage
    public void goToInferno (){
        driver.get(baseURL);
        //driver.navigate().to(baseURL)
    }

    //Go to LoginPage
    public void goToLoginPage (){
        driver.get(baseURL + "/Account/Login");
    }
}