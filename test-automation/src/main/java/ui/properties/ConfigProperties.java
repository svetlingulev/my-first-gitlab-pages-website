package ui.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class ConfigProperties {

    private static Properties properties;

    static {
        properties = new Properties();
        try {
            File file = new File("src/main/resources/config.properties");
            file.exists();
            FileInputStream in = new FileInputStream(file);
            properties.load(in);
            in.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("config.properties file is missing.", e);
        }
    }

    public static String username = properties.getProperty("username");
    public static String password = properties.getProperty("password");
    public static String url = properties.getProperty("url");
    public static String createPolls = properties.getProperty("createPolls");
    public static String userAdmin = properties.getProperty("userAdmin");
    public static String mail = properties.getProperty("mail");
    public static String emailPass = properties.getProperty("emailpass");
    public static String mailUrl = properties.getProperty("mailUrl");
    public static String userkris = properties.getProperty("userkris");
    public static String userGumball = properties.getProperty("userGumball");
    public static String defaultBrowser = properties.getProperty("defaultBrowser");


}