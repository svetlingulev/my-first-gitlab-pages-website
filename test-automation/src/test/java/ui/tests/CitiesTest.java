package ui.tests;

import ui.pages.CitiesPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CitiesTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
     //   login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addCity() throws InterruptedException {

        CitiesPage cities = new CitiesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        cities.clickCities();
        Thread.sleep(1000);
        cities.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD CITY");
        cities.addCountry();
        cities.typeName("A test city");
        cities.typeZIP("1000");
        Thread.sleep(500);
        cities.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "City was saved");
    }

    @Test(priority = 2)
    public void editCities() throws InterruptedException {

        CitiesPage cities = new CitiesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[23]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        cities.clickCities();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Cities");
        Thread.sleep(500);
        cities.clickEdit();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "EDIT CITY");
        cities.editName();
        Thread.sleep(500);
        cities.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "City was saved");
    }

    @Test(priority = 3)
    public void deleteCities() throws InterruptedException {

        CitiesPage cities = new CitiesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[23]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        cities.clickCities();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Cities");
        Thread.sleep(500);
        cities.clickOnDeleteButton();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "City was deleted");

    }

}


