package ui.tests;

import ui.pages.ConfigurationSystemPage;
import ui.pages.CustomUserFieldsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CustomUserFieldsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
       // assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addCustomUserFields() throws InterruptedException {

        CustomUserFieldsPage userProfile = new CustomUserFieldsPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        userProfile.clickOnCustomUser();
        Thread.sleep(1000);
        userProfile.clickOnAddNew();
        Thread.sleep(1000);
        userProfile.typeName("test");
        userProfile.typeLabelName("test");
        userProfile.attributeTypeDD();
        userProfile.clickOnSave();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Custom Attribute was saved");
    }

    @Test(priority = 2)
    public void listCustomUserFields() throws InterruptedException {

        CustomUserFieldsPage userProfile = new CustomUserFieldsPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        userProfile.clickOnCustomUser();
        Thread.sleep(1000);
        userProfile.typeName("test");
        userProfile.typeLabelName("test");
        Thread.sleep(1000);
        userProfile.listAttributeTypeDD();
        userProfile.clickOnSearch();
        String Text = driver.findElement(By.xpath(".//*[@id='customattributes-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "test");
    }

    @Test(priority = 3)
    public void editCustomUserFields() throws InterruptedException {

        CustomUserFieldsPage userProfile = new CustomUserFieldsPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        userProfile.clickOnCustomUser();
        Thread.sleep(1000);
        userProfile.typeName("test");
        userProfile.typeLabelName("test");
        Thread.sleep(1000);
        userProfile.listAttributeTypeDD();
        userProfile.clickOnSearch();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='customattributes-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "test");
        Thread.sleep(1000);
        userProfile.clickOnEdit();
        userProfile.typeEditedName();
        Thread.sleep(1000);
        userProfile.clickOnSave();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Custom Attribute was saved");

    }
    @Test(priority = 4)
    public void deleteCustomUserFields() throws InterruptedException {

        CustomUserFieldsPage userProfile = new CustomUserFieldsPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        userProfile.clickOnCustomUser();
        Thread.sleep(1000);
        userProfile.typeName("edited name");
        userProfile.typeLabelName("test");
        Thread.sleep(1000);
        userProfile.listAttributeTypeDD();
        userProfile.clickOnSearch();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='customattributes-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "edited name");
        Thread.sleep(1000);
        userProfile.clickOnDelete();
        Thread.sleep(1000);
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Custom Attribute was deleted");

    }

}
