package ui.tests;

import ui.properties.ConfigProperties;
import org.testng.annotations.Test;
import ui.pages.LoginPage;
import ui.pages.HomePage;

import java.io.IOException;

public class LoginTest extends BaseTest {


    @Test (priority = 0)
    public void openInferno () throws IOException {

        //*************PAGE INSTANTIATIONS*************
        HomePage homePage = new HomePage(driver,wait);
        LoginPage loginPage = new LoginPage(driver,wait);
        ConfigProperties cfg = new ConfigProperties();


        //*************PAGE METHODS********************
        //Open inferno HomePage
        homePage.goToInferno();


        //Go to LoginPage
        homePage.goToLoginPage();
        loginPage.loginToInferno(cfg.username,cfg.password);


        //*************ASSERTIONS***********************


    }

}