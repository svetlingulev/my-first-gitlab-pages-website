package ui.tests;

import ui.pages.UserPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


/**
 * Created by ddzhonova on 28.10.16.
 */
public class ProgramistaLoginTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");


    @BeforeClass
    public void beforeClass() throws IOException {
        props.load(new FileInputStream(file));

        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");

    }

//    @AfterClass
//    public void afterClass() {
//    driver.quit();
//    }


    @Test(priority = 1)
    public void addUser() throws InterruptedException {

        UserPage user = new UserPage(driver);
        Thread.sleep(1000);
        user.clickOnUserButton();
        Thread.sleep(2000);
        user.ClickOnAddUsersButton();
        user.typeUsername("hr");
        user.typeFName("desi");
        user.typeLName("dzhonova");
        user.typeFullName("desi dzhonova");
        user.addUserDetailsDD();
        user.clickOnClassification();
        Thread.sleep(1000);
        user.typeClassification("а");
        Thread.sleep(1000);
        user.chooseClassification();
        user.typePassword("PIpanoid_00_00_00");
        user.typeRepeatPassword("PIpanoid_00_00_00");
        user.typePhone("359898301120");
        user.typeEmail("dd@hh.bg");
        user.addCalendar();
        user.clickonBusinessInfoButton();
        user.addBusinessInfo();
        String ActualText=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(ActualText, "User successfully saved");
    }



    @Test(priority = 2)
    public void verifyUserCreated() throws InterruptedException {

        UserPage user = new UserPage(driver);
        Thread.sleep(1000);
        user.clickOnUserButton();
        user.typeFirstName("Desi");
        user.ClickonSearchButton();
        String ActualText=driver.findElement(By.xpath(".//*[@id='user-list']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(ActualText, "desi dzhonova");
    }


    @Test(priority = 3)
    public void verifyNonexistentUser() throws InterruptedException {

        UserPage user = new UserPage(driver);
        Thread.sleep(1000);
        user.clickOnUserButton();
        user.typeFirstName("test");
        user.ClickonSearchButton();
        String ActualText=driver.findElement(By.xpath(".//*[@id='formItems']/div[2]/div")).getText();
        Assert.assertEquals(ActualText, "No users");
    }

    @Test(priority = 4)
    public void editUser() throws InterruptedException {

        UserPage user = new UserPage(driver);
        Thread.sleep(1000);
        user.clickOnUserButton();
        user.typeFirstName("Desi");
        user.ClickonSearchButton();
        String ActualText=driver.findElement(By.xpath(".//*[@id='user-list']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(ActualText, "desi dzhonova");
        Thread.sleep(2000);
        user.ClickOnEditutton();
        user.editUserDetailsDD();
        user.editEmail();
        user.editPhone();
        user.clickonBusinessInfoButton();
        user.editBusinessInfo();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "User successfully edited.");
    }

    @Test(priority = 5)
    public void deleteUser() throws InterruptedException {

        UserPage user = new UserPage(driver);
        Thread.sleep(1000);
        user.clickOnUserButton();
        user.typeFirstName("Desi");
        user.ClickonSearchButton();
        String ActualText=driver.findElement(By.xpath(".//*[@id='user-list']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(ActualText, "desi dzhonova");
        Thread.sleep(2000);
        user.ClickOnDeleteButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "User successfully deleted");
    }

}
