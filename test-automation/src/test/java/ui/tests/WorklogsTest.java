package ui.tests;

import ui.pages.WorklogsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorklogsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addNormalLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
        Thread.sleep(1000);
        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickTypeButton2();
        Thread.sleep(1000);
        worklog.editProject();
        worklog.startDate3();
        worklog.endDate3();
        Thread.sleep(1000);
        worklog.typeLoggedTime();
        worklog.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Successfully added worklog");
    }

    @Test(priority = 2)
    public void addOvertimeLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickTypeButton();
        Thread.sleep(1000);
        worklog.editProject();
        worklog.startDate3();
        worklog.endDate3();
        Thread.sleep(1000);
        worklog.typeLoggedTime();
        worklog.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Successfully added overtime");
    }
    @Test(priority = 3)
    public void approveOvertime() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
     //   LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userAdmin"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        worklog.clickWorkLogs();
        worklog.clickOnManageOvertime();
        String ActualText=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(ActualText, "Admin Admin");
        Thread.sleep(1000);
        worklog.clickOnReject();
        String Text3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Successfully rejected overtime");
        Thread.sleep(1000);
        worklog.clickOnApprove();
        String ActualText2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(ActualText2, "Successfully approved overtime");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        worklog.clickWorkLogs();
        worklog.clickOnMyWorkLogs();
        Thread.sleep(1000);
        String ActualText3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[2]/td[4]")).getText();
        Assert.assertEquals(ActualText3, "Overtime");

    }

    @Test(priority = 4)
    public void addCanNotLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickTypeButton2();
        Thread.sleep(1000);
        worklog.editProject();
        worklog.startDate3();
        worklog.endDate3();
        Thread.sleep(1000);
        worklog.typeLoggedTime();
        worklog.clickSave();
        Thread.sleep(1000);
//        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/div/ul/li")).getText();
//        Assert.assertEquals(Text, "You have logged 8 hours for 2016-12-22 that doesn't allow you to log more hours for that day.");
    }

    @Test(priority = 5)
    public void addHolidayLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickTypeButton2();
        Thread.sleep(1000);
        worklog.editProject();
        worklog.startDate2();
        worklog.endDate2();
        Thread.sleep(1000);
        worklog.typeLoggedTime();
        worklog.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[2]/div/ul/li")).getText();
        Assert.assertEquals(Text, "You can not add worklogs for non-working days.");
    }

    @Test(priority = 8)
    public void deleteNormalLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
//        Thread.sleep(1000);
//        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickOnMyWorkLogs();
        Thread.sleep(1000);
        worklog.clickApprovedWorklogs();
        String ActualText=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[4]")).getText();
        Assert.assertEquals(ActualText, "Normal");
        Thread.sleep(1000);
        worklog.clickOnDeleteButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Successfully deleted worklog");
    }

    @Test(priority = 9)
    public void deleteOvertimeLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
//        Thread.sleep(1000);
//        worklog.clickLogWork();
        Thread.sleep(1000);
        worklog.clickOnMyWorkLogs();
        Thread.sleep(1000);
        worklog.clickApprovedWorklogs();
        Thread.sleep(1000);
        String ActualText=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[4]")).getText();
        Assert.assertEquals(ActualText, "Overtime");
        Thread.sleep(1000);
        worklog.clickOnDeleteButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Successfully deleted worklog");
    }

    @Test(priority = 6)
    public void listLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickListWorklogs();
        Thread.sleep(1000);
        worklog.startDate3();
        worklog.endDate3();
        worklog.clickOnSearch();
        String ActualText=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[2]/td[4]")).getText();
        Assert.assertEquals(ActualText, "Admin");

    }

    @Test(priority = 7)
    public void editNormalLogWork() throws InterruptedException {

        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        worklog.clickWorkLogs();
        Thread.sleep(1000);
        worklog.clickOnMyWorkLogs();
        Thread.sleep(1000);
        worklog.clickApprovedWorklogs();
        String ActualText=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[4]")).getText();
        Assert.assertEquals(ActualText, "Normal");
        Thread.sleep(1000);
        worklog.clickOnEdit();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "EDIT WORKLOG");
        worklog.typeNewLoggedTime();
        Thread.sleep(1000);
        worklog.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Successfully edited worklog");
    }

}
