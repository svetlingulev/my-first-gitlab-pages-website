package ui.tests;


import ui.pages.HolidayPage;
import ui.pages.WorkdaysPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorkdaysTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addWorkday() throws InterruptedException {

        WorkdaysPage workday = new WorkdaysPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        workday.clickWorkdays();
        Thread.sleep(1000);
        workday.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "CREATE WORKDAY");
        workday.typeWorkdayName("a working day");
        Thread.sleep(500);
        workday.workdayDate();
        workday.clickSynchronize();
        workday.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Workday was saved");
    }

    @Test(priority = 2)
    public void listWorkday() throws InterruptedException {

        WorkdaysPage workday = new WorkdaysPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        workday.clickWorkdays();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Work days");
        Thread.sleep(500);
        workday.findByYearDD();
        Thread.sleep(500);
        String Text1=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text1, "a working day");
    }

    @Test(priority = 3)
    public void editWorkday() throws InterruptedException {

        WorkdaysPage workday = new WorkdaysPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        workday.clickWorkdays();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Work days");
        Thread.sleep(500);
        workday.findByYearDD();
        Thread.sleep(500);
        String Text1=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text1, "a working day");
        Thread.sleep(500);
        workday.clickEdit();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text2, "EDIT WORKDAY");
        Thread.sleep(500);
        workday.editedName();
        workday.clickSave();
        String Text3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Workday was saved");
    }

    @Test(priority = 4)
    public void deleteWorkday() throws InterruptedException {

        WorkdaysPage workday = new WorkdaysPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        workday.clickWorkdays();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Work days");
        Thread.sleep(500);
        workday.findByYearDD();
        Thread.sleep(500);
        String Text1=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text1, "editedWorkday");
        Thread.sleep(500);
        workday.clickOnDeleteButton();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Work day was deleted");

    }

}
