package ui.tests;

import ui.pages.TasksPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.testng.Assert.assertTrue;

/**
 * Created by ddzhonova on 17.11.16.
 */
public class TasksTest {

    private WebDriver driver;
    Properties props = new Properties();
//    Logger logger = ne
    File file = new File("src/main/resources/config.properties");


    @BeforeClass
    public void beforeClass() throws IOException {
        props.load(new FileInputStream(file));

//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
//        this.driver = new ChromeDriver();
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }


    @Test(priority = 1)
    public void addTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
    }

    @Test(priority = 2)
    public void listTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "testTitle3");
    }

    @Test(priority = 3)
    public void editTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "testTitle3");
        Thread.sleep(1000);
        tasks.clickEditButton();
        tasks.editTitle();
        Thread.sleep(1000);
        tasks.clickSubmitButton();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text2, "Task was saved");
    }

    @Test(priority = 10)
    public void deleteTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("editedTitle");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "editedTitle");
        Thread.sleep(2000);
        tasks.clickOnDeleteButton();
        String Text2 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text2, "Task was deleted");
    }

    @Test(priority = 4)
    public void completeTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("editedTitle");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "editedTitle");
        Thread.sleep(1000);
        tasks.clickCompleteButton();
      String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text2, "Status changed");
    }

    @Test(priority = 5)
    public void reopenTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("editedTitle");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "editedTitle");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text2, "Status changed");
    }


    @Test(priority = 11)
    public void inProgressTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(500);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(2000);
        tasks.clickOnDeleteButton();
        String Text4 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text4, "Task was deleted");

    }

    @Test(priority = 12)
    public void rejectTasks() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        tasks.clickReject();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text4 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text4, "Task was deleted");

    }

    @Test(priority = 13)
    public void toDoTasksInProgress() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(500);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(1000);
        tasks.clickToDo();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        Thread.sleep(2000);
        tasks.clickOnDeleteButton();
        String Text5 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text5, "Task was deleted");

    }

    @Test(priority = 14)
    public void reviewTasksInProgress() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(500);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(1000);
        tasks.clickReview();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        Thread.sleep(2000);
        tasks.clickOnDeleteButton();
        String Text5 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text5, "Task was deleted");

    }

    @Test(priority = 15)
    public void rejectTasksInProgress() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(500);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(1000);
        tasks.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text6 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text6, "Task was deleted");

    }

    @Test(priority = 16)
    public void inReviewReopen() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(500);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(1000);
        tasks.clickReview();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text6 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text6, "Task was deleted");

    }

    @Test(priority = 17)
    public void rejectReopen() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(1000);
        tasks.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text6 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text6, "Task was deleted");

    }

    @Test(priority = 18)
    public void doneReopen() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(1000);
        tasks.clickCompleteButton();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(500);
        tasks.clickSearchButton();
        Thread.sleep(500);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Done");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text6 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text6, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 19)
    public void toDoReject() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(1000);
        tasks.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
//        driver.get(props.getProperty("mailUrl"));
//        LoginPage login = new LoginPage(driver);
//        login.clickOnMailLogin();
//        login.typeEmail(props.getProperty("mail"));
//        login.typeMailPassword(props.getProperty("emailpass"));
//        login.clickLogIn();
//        tasks.clickDemoInbox();
//        Thread.sleep(1000);
//        tasks.clickOnTaskDeletedMail();
//        String Text8 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text8, "Task deleted");
//        tasks.clickOnTaskEditedMail();
//        String Text9 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text9, "Task edited");
//        tasks.clickOnAssignedTask();
//        String Text10 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text10, "New task assigned to you");
    }

    @Test(priority = 20)
    public void toDoComplete() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='tasks-list']/table/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "testTitle3");
        Thread.sleep(1000);
        tasks.clickCompleteButton();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Done");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
//        driver.get(props.getProperty("mailUrl"));
//        LoginPage login = new LoginPage(driver);
//        login.clickOnMailLogin();
//        login.typeEmail(props.getProperty("mail"));
//        login.typeMailPassword(props.getProperty("emailpass"));
//        login.clickLogIn();
//        tasks.clickDemoInbox();
//        Thread.sleep(1000);
//        tasks.clickOnTaskDeletedMail();
//        String Text8 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text8, "Task deleted");
//        tasks.clickOnTaskEditedMail();
//        String Text9 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text9, "Task edited");
//        tasks.clickOnAssignedTask();
//        String Text10 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text10, "New task assigned to you");
    }

    @Test(priority = 21)
    public void inProgressRejectUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.clickOnAdminButton();
       // LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userkris"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        Thread.sleep(1000);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "Status changed");
        Thread.sleep(1000);
        tasks.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text6 = driver.findElement(By.xpath("//p[text()='Task was deleted']")).getText();
        Assert.assertEquals(Text6, "Task was deleted");
//        driver.get(props.getProperty("mailUrl"));
//        login.clickOnMailLogin();
//        login.typeEmail(props.getProperty("mail"));
//        login.typeMailPassword(props.getProperty("emailpass"));
//        login.clickLogIn();
//        tasks.clickDemoInbox();
//        Thread.sleep(1000);
//        tasks.clickOnTaskDeletedMail();
//        String Text8 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text8, "Task deleted");
//        tasks.clickOnTaskEditedMail();
//        String Text9 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text9, "Task edited");
//        tasks.clickOnTaskEditedMail2();
//        String Text10 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text10, "Task edited");
//        tasks.clickOnAssignedTask2();
//        String Text11 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
//        Assert.assertEquals(Text11, "New task assigned to you");
    }

    @Test(priority = 22)
    public void inReviewReopenUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.clickOnAdminButton();
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userkris"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        Thread.sleep(1000);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text3, "In Progress");
        Thread.sleep(1000);
        tasks.clickReview();
        String Text4 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text4, "In Review");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 23)
    public void inReviewCompleteUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.clickOnAdminButton();
     //   LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userkris"));
     //   login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        Thread.sleep(1000);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text3, "In Progress");
        Thread.sleep(1000);
        tasks.clickReview();
        String Text4 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text4, "In Review");
        tasks.clickOnAdminButton();
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        tasks.clickCompleteButton();
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Done");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 24)
    public void rejectedReopenUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.clickOnAdminButton();
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("userkris"));
      //  login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        Thread.sleep(1000);
        tasks.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text4, "Status changed");
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text6 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text6, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 25)
    public void toDoneReopen() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        tasks.clickCompleteButton();
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Done");
        Thread.sleep(1000);
        tasks.clickReopenButton();
        String Text6 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text6, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 26)
    public void toDoStartUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(1000);
        tasks.clickOnAdminButton();
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userkris"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        Thread.sleep(1000);
        tasks.startButton();
        String Text3 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text3, "In Progress");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");
    }

    @Test(priority = 27)
    public void toDoRejectUser() throws InterruptedException {

        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        tasks.clickTasks();
        Thread.sleep(1000);
        tasks.clickAddNewButton();
        tasks.typeTitle("testTitle3");
        tasks.selectAssignToDepartment();
        Thread.sleep(1000);
        tasks.selectAssignToUser();
        tasks.typeDescription("test");
        tasks.addPriorityDD();
        tasks.dueDate();
        tasks.clickSaveButton();
        Thread.sleep(1000);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Task was saved");
        Thread.sleep(500);
        tasks.clickOnAdminButton();
       // LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userkris"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        tasks.clickTasks();
        tasks.TypeTitle2("testTitle3");
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text2, "To Do");
        tasks.clickReject();
        tasks.statusDD();
        Thread.sleep(1000);
        tasks.clickSearchButton();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='table-tasks']/tbody/tr[1]/td[2]/span")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        tasks.clickOnDeleteButton();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Task was deleted");

    }

}
