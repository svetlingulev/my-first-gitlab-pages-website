package ui.tests;

import ui.pages.*;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.ConfigurationSystemPage;
import ui.pages.FeedPage;
import ui.pages.TasksPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class FeedsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addFeed() throws InterruptedException {

        FeedPage feed = new FeedPage(driver);
        Thread.sleep(1000);
        feed.clickNewsFeed();
        Thread.sleep(2000);
        feed.clickAddNewButton();
        feed.addActiveDate();
        feed.typeTitle("test feed");
        Thread.sleep(1000);
        feed.addTargetCompanies();
        feed.typeDescription("test feed");
        feed.typeContent("test feed");
        feed.clickIsSticky();
        feed.clickIsActive();
        feed.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "News Feed was saved");
    }

    @Test(priority = 2)
    public void listFeed() throws InterruptedException {

        FeedPage feed = new FeedPage(driver);
        Thread.sleep(1000);
        feed.clickNewsFeed();
        feed.fromDate();
        Thread.sleep(1000);
        feed.toDate();
        feed.typeAuthor("Admin Admin");
        feed.typeTitle2("test feed");
        feed.selectStatus();
        feed.clickSearch();
        String Text=driver.findElement(By.xpath(".//*[@id='jobOffers-list']/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text, "test feed");
    }

    @Test(priority = 3)
    public void editFeed() throws InterruptedException {

        FeedPage feed = new FeedPage(driver);
        Thread.sleep(1000);
        feed.clickNewsFeed();
        feed.typeTitle2("test feed");
        Thread.sleep(2000);
        feed.clickSearch();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath(".//*[@id='jobOffers-list']/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text, "test feed");
        Thread.sleep(2000);
        feed.clickEdit();
        feed.editTitle();
        Thread.sleep(1000);
        feed.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text2, "News Feed was saved");
    }


    @Test(priority = 4)
    public void deleteFeed() throws InterruptedException {

        FeedPage feed = new FeedPage(driver);
        Thread.sleep(1000);
        feed.clickNewsFeed();
        feed.typeTitle2("edited feed");
        Thread.sleep(2000);
        feed.clickSearch();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath(".//*[@id='jobOffers-list']/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text, "edited feed");
        Thread.sleep(2000);
        feed.clickDelete();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text2, "News Feed was deleted");
    }

    @Test(priority = 5)
    public void addFeedCheckMail() throws InterruptedException {

        FeedPage feed = new FeedPage(driver);
        TasksPage tasks = new TasksPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        feed.clickNewsFeed();
        Thread.sleep(2000);
        feed.clickAddNewButton();
        feed.addActiveDate();
        feed.typeTitle("test feed");
        Thread.sleep(1000);
        feed.addTargetCompanies();
        feed.typeDescription("test feed");
        feed.typeContent("test feed");
        feed.clickIsSticky();
        feed.clickIsActive();
        feed.clickSaveAndSend();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text, "News Feed was saved");
        feed.fromDate();
        Thread.sleep(1000);
        feed.toDate();
        feed.typeAuthor("Admin Admin");
        feed.typeTitle2("test feed");
        feed.selectStatus();
        feed.clickSearch();
        String Text2=driver.findElement(By.xpath(".//*[@id='jobOffers-list']/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text2, "test feed");
        Thread.sleep(2000);
        feed.clickDelete();
        String Text3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]")).getText();
        Assert.assertEquals(Text3, "News Feed was deleted");
        driver.get(props.getProperty("mailUrl"));
     //   LoginPage login = new LoginPage(driver);
      //  login.clickOnMailLogin();
       // login.typeEmail(props.getProperty("mail"));
      //  login.typeMailPassword(props.getProperty("emailpass"));
      //  login.clickLogIn();
        Thread.sleep(2000);
        tasks.clickDemoInbox();
        Thread.sleep(1000);
        system.clickOnTestMail();
        String Text4 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
        Assert.assertEquals(Text4, "News");
    }

}
