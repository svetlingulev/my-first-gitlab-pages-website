package ui.tests;

import ui.pages.PollsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by ddzhonova on 09.11.16.
 */
public class PollsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");


    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));

//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
     //   login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }
//
//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }


    @Test
    public void addPolls() throws InterruptedException {

        PollsPage polls = new PollsPage(driver);
        driver.get(props.getProperty("createPolls"));
        Thread.sleep(1000);
        String ActualText=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(ActualText, "ADD POLL");
        polls.addPollDDValues();
        polls.typetitle("testTitle");
        polls.addFromDate();
        polls.addEndDtae();
        Thread.sleep(1000);
        polls.addCompanyDDValues();
//        polls.addCitiesDDValues();
        polls.answer1("test1");
        polls.answer2("test2");
        polls.clickonResultIsPublicButton();
        polls.clickonSaveButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Poll was saved");
    }

    @Test
    public void listPolls() throws InterruptedException {

        PollsPage polls = new PollsPage(driver);
        Thread.sleep(1000);
        polls.clickOnPollsButton();
//        polls.addFromDtaeListPolls();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        polls.Title("testTitle");
        polls.Author("Admin");
        polls.selectDDStatus();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("scroll(0, -250);");
        Thread.sleep(2000);
//        polls.addCompanyDDValues();
//        polls.addCitiesDDValues();
        polls.clickonSearchButton();
        String Text=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[3]")).getText();
        Assert.assertEquals(Text, "testTitle");
    }

    @Test
    public void editPolls() throws InterruptedException {

        PollsPage polls = new PollsPage(driver);
        Thread.sleep(1000);
        polls.clickOnPollsButton();
//        polls.addFromDtaeListPolls();
        polls.Title("testTitle");
        Thread.sleep(2000);
        polls.clickonSearchButton();
        String Text=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[3]")).getText();
        Assert.assertEquals(Text, "testTitle");
        Thread.sleep(2000);
        polls.clickonEditButton();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text2, "EDIT POLL");
        Thread.sleep(1000);
        polls.addFromDate();
        polls.addEndDtae();
//        polls.addCompanyDDValues();
//        polls.addCitiesDDValues();
        polls.clickonSaveButton();
        String Text3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Poll was saved");
    }

    @Test
    public void votePolls() throws InterruptedException {

        PollsPage polls = new PollsPage(driver);
        Thread.sleep(1000);
        polls.clickOnPollsButton();
//        polls.addFromDtaeListPolls();
        polls.Title("testTitle");
        Thread.sleep(2000);
        polls.clickonSearchButton();
        String Text=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[3]")).getText();
        Assert.assertEquals(Text, "testTitle");
        polls.clickonVoteButton();
        Thread.sleep(1000);
        polls.checkVote();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Thank you for voting.");
        Thread.sleep(1000);
        polls.clickonVoteButton();
        String Text3=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/form/div/div[2]")).getText();
        Assert.assertEquals(Text3, "You have already voted.");
        Thread.sleep(1000);
        polls.clickonBackToList();
        String Text4=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text4, "TESTTITLE");
    }

    @Test(priority = 100)
    public void deletePolls() throws InterruptedException {

        PollsPage polls = new PollsPage(driver);
        Thread.sleep(1000);
        polls.clickOnPollsButton();
//        polls.addFromDtaeListPolls();
        polls.Title("testTitle");
        Thread.sleep(1000);
        polls.clickonSearchButton();
        String Text=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[3]")).getText();
        Assert.assertEquals(Text, "testTitle");
        Thread.sleep(1000);
        polls.clickonDeleteButton();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Poll was deleted");
    }

}
