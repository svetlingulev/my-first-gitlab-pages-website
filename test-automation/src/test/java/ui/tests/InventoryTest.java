package ui.tests;

import ui.pages.*;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.InventoryPage;
import ui.pages.TasksPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class InventoryTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addInventory() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
        Thread.sleep(1000);
        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItemTypes();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Item Types");
        Thread.sleep(500);
        inventory.clickOnAddNew();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "ADD ITEM TYPE");
        inventory.executiveGroupDD();
        inventory.typeName("tete");
        inventory.typeYears("10");
        Thread.sleep(500);
        inventory.clickSave();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Item Type was saved");

    }

    @Test(priority = 2)
    public void editInventory() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItemTypes();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Item Types");
        Thread.sleep(500);
        inventory.selectGroupDD();
        inventory.listType("tete");
        inventory.approval();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath("//td[text()='tete']")).getText();
        Assert.assertEquals(Text1, "tete");
        Thread.sleep(500);
        inventory.clickEdit();
        inventory.typeEditedInventory();
        Thread.sleep(500);
        inventory.clickSave();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Item Type was saved");
    }

    @Test(priority = 3)
    public void deleteInventory() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(1000);
        inventory.clickItemTypes();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Item Types");
        Thread.sleep(500);
        inventory.selectGroupDD();
        inventory.listType("edittete");
        inventory.approval();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath("//td[text()='edittete']")).getText();
        Assert.assertEquals(Text1, "edittete");
        Thread.sleep(500);
        inventory.clickOnDelete();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Item Type was deleted");
    }
    @Test(priority = 4)
    public void addItem() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItems();
        Thread.sleep(1000);
        inventory.clickOnAddNew();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD ITEM");
        Thread.sleep(1000);
        inventory.itemTypeDD();
        inventory.typeInventoryNumber("1234567"+new java.util.Date().getTime());
        inventory.typeModel("test model");
        inventory.typeSerialNumber("qwerty999");
        inventory.addComponents();
        inventory.assignedUserDD();
        inventory.countryDD();
        inventory.cityDD();
        inventory.locationDD();
        Thread.sleep(500);
        inventory.locationUnitDD();
        inventory.typeSupplier("test Supplier");
        inventory.typePrice("30");
        inventory.buyDate();
        inventory.typeComment("test");
        inventory.clickSave();
        String Text1 = driver.findElement(By.xpath("//p[text()='Item was saved']")).getText();
        Assert.assertEquals(Text1, "Item was saved");
    }

    @Test(priority = 5)
    public void listItem() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItems();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]")).getText();
        Assert.assertEquals(Text, "Items");
        Thread.sleep(1000);
        inventory.itemTypeDD();
        inventory.listCountryDD();
        inventory.listAssignedUserDD();
        inventory.cityDD();
        inventory.listModel("test model");
        inventory.locationDD();
        inventory.listSerialNumber("qwerty999");
        inventory.locationUnitDD();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath(".//*[@id='Items']/table/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text1, "test model");
    }

    @Test(priority = 7)
    public void itemsDetails() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItems();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]")).getText();
        Assert.assertEquals(Text, "Items");
        Thread.sleep(1000);
        inventory.itemTypeDD();
        inventory.listCountryDD();
        inventory.listAssignedUserDD();
        inventory.cityDD();
        inventory.listModel("edited name");
        inventory.locationDD();
        inventory.listSerialNumber("qwerty999");
        inventory.locationUnitDD();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath(".//*[@id='Items']/table/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text1, "edited name");
        Thread.sleep(500);
        inventory.clickDetails();
        Thread.sleep(1000);
        inventory.clickClose();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]")).getText();
        Assert.assertEquals(Text2, "Items");
    }
    @Test(priority = 6)
    public void editItems() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItems();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]")).getText();
        Assert.assertEquals(Text, "Items");
        Thread.sleep(1000);
        inventory.itemTypeDD();
        inventory.listCountryDD();
        inventory.listAssignedUserDD();
        inventory.cityDD();
        inventory.listModel("test model");
        inventory.locationDD();
        inventory.listSerialNumber("qwerty999");
        inventory.locationUnitDD();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath(".//*[@id='Items']/table/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text1, "test model");
        Thread.sleep(500);
        inventory.clickItemsEdit();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]")).getText();
        Assert.assertEquals(Text2, "EDIT ITEM");
        Thread.sleep(1000);
        inventory.editedModel();
        inventory.clickSave();
        String Text3 = driver.findElement(By.xpath("//p[text()='Item was saved']")).getText();
        Assert.assertEquals(Text3, "Item was saved");

    }

    @Test(priority = 8)
    public void deleteItems() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItems();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]")).getText();
        Assert.assertEquals(Text, "Items");
        Thread.sleep(1000);
        inventory.itemTypeDD();
        inventory.listCountryDD();
        inventory.listAssignedUserDD();
        inventory.cityDD();
        inventory.listModel("edited name");
        inventory.locationDD();
        inventory.listSerialNumber("qwerty999");
        inventory.locationUnitDD();
        inventory.clickSearch();
        String Text1 = driver.findElement(By.xpath(".//*[@id='Items']/table/tbody/tr/td[3]")).getText();
        Assert.assertEquals(Text1, "edited name");
        Thread.sleep(500);
        inventory.clickItemsDelete();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Item was deleted");

    }

    @Test(priority = 9)
    public void approveRequestedItem() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
        TasksPage tasks = new TasksPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.clickItemTypes();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Items Types");
        Thread.sleep(500);
        inventory.clickOnAddNew();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "ADD ITEM TYPE");
        inventory.executiveGroupDD();
        inventory.typeName("test");
        inventory.typeYears("10");
        Thread.sleep(500);
        inventory.clickSave();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Item Type was saved");
        Thread.sleep(500);
        inventory.clickMyItemsRequestst();
        Thread.sleep(500);
        inventory.requestAnItem();
        inventory.addType();
        inventory.typeDescription("test");
        Thread.sleep(500);
        inventory.request();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Item request was saved");
        Thread.sleep(1000);
        tasks.clickOnAdminButton();
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("userAdmin"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        Thread.sleep(500);
        inventory.clickInventory();
        Thread.sleep(500);
        inventory.userItemRequest();
        Thread.sleep(500);
        inventory.approveRequets();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Item request was approved");

    }

    @Test(priority = 10)
    public void takeDeliverItems() throws InterruptedException {

        InventoryPage inventory = new InventoryPage(driver);
//        Thread.sleep(1000);
//        inventory.clickInventory();
        Thread.sleep(500);
        inventory.executiveGroupRequests();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "Items requests");
        Thread.sleep(500);
        inventory.clickTake();
        Thread.sleep(500);
        inventory.clickDeliver();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text2, "SET ITEM'S INFORMATION");
        Thread.sleep(500);
        inventory.locationDD();
        Thread.sleep(500);
        inventory.locationUnitDD();
        inventory.typeSupplier("test");
        inventory.typeModel("test");
        inventory.buyDate();
        inventory.typePrice("30");
        inventory.typeInventoryNumber("1234567" + new java.util.Date().getTime());
        inventory.clickSave();
        Thread.sleep(1000);
        inventory.clickMyItemsRequestst();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text3, "My items requests");
        Thread.sleep(1000);
        inventory.clickReceive();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Item was recieved");

    }


}

