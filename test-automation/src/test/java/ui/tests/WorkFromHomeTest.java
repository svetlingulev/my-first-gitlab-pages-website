package ui.tests;

import ui.pages.WorkFromHomePage;
import ui.pages.WorklogsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class WorkFromHomeTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void requestWorkFromHome() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
        Thread.sleep(1000);
        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickRequets();
        Thread.sleep(1000);
        workFromHome.startDate();
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.endDate();
        Thread.sleep(1000);
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.typeLoggedHours();
        workFromHome.clickSaveRequest();
        Thread.sleep(1000);
        workFromHome.clickConfirmRequest();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Work from home request was saved");

    }

    @Test(priority = 2)
    public void dublicateRequest() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
//        Thread.sleep(1000);
//        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickRequets();
        Thread.sleep(1000);
        workFromHome.startDate();
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.endDate();
        Thread.sleep(1000);
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.typeLoggedHours();
        workFromHome.clickSaveRequest();
        Thread.sleep(1000);
        workFromHome.clickConfirmRequest();
//        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/ul/li")).getText();
//        Assert.assertEquals(Text, "There are more than allowed logged Work From Home hours for 2016-12-20 that doesn't allow you to make this request.");

    }

    @Test(priority = 3)
    public void weekendRequest() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
//        Thread.sleep(1000);
//        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickRequets();
        Thread.sleep(1000);
        workFromHome.startDateWeekend();
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.endDateWeekend();
        Thread.sleep(1000);
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.clickSaveRequest();
        Thread.sleep(1000);
        workFromHome.clickConfirmRequest();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/ul/li")).getText();
        Assert.assertEquals(Text, "You must have at least one working day in the selected period");

    }

    @Test(priority = 4)
    public void deleteRequest() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
//        Thread.sleep(1000);
//        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickMyRequest();
        Thread.sleep(1000);
        String Text3=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[5]")).getText();
        Assert.assertEquals(Text3, "Pending");
        Thread.sleep(1000);
        workFromHome.clickOnDeleteButton();
        String Text4=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Work from home request was deleted");
    }

    @Test(priority = 5)
    public void rejectRequests() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickRequets();
        Thread.sleep(1000);
        workFromHome.startDate();
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.endDate();
        Thread.sleep(1000);
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.typeLoggedHours();
        workFromHome.clickSaveRequest();
        Thread.sleep(1000);
        workFromHome.clickConfirmRequest();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Work from home request was saved");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
     //   LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userAdmin"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickManageRequests();
        Thread.sleep(1000);
        String ActualText=driver.findElement(By.xpath(".//*[@id='pending ptos']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(ActualText, "Admin Admin");
        Thread.sleep(1000);
        worklog.clickOnReject();
        Thread.sleep(1000);
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Work from home request was rejected");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
     //   login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickMyRequest();
        Thread.sleep(1000);
        String Text3=driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[5]")).getText();
        Assert.assertEquals(Text3, "Rejected");
        Thread.sleep(1000);
        workFromHome.clickOnDeleteButton();
        String Text4=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Work from home request was deleted");

    }

    @Test(priority = 6)
    public void approveRequests() throws InterruptedException {

        WorkFromHomePage workFromHome = new WorkFromHomePage(driver);
        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickRequets();
        Thread.sleep(1000);
        workFromHome.startDate();
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.endDate();
        Thread.sleep(1000);
        workFromHome.clickSomewhere();
        Thread.sleep(1000);
        workFromHome.typeLoggedHours();
        workFromHome.clickSaveRequest();
        Thread.sleep(1000);
        workFromHome.clickConfirmRequest();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Work from home request was saved");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("userAdmin"));
      //  login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        Thread.sleep(1000);
        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickManageRequests();
        Thread.sleep(1000);
        String ActualText=driver.findElement(By.xpath(".//*[@id='pending ptos']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(ActualText, "Admin Admin");
        Thread.sleep(1000);
        workFromHome.clickApprove();
        Thread.sleep(1000);
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Work from home request was approved");
        Thread.sleep(1000);
        worklog.clickOnReject();
        Thread.sleep(1000);
        worklog.clickOnRejectConfirm();
        Thread.sleep(1000);
        String Text4=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Work from home request was rejected");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        workFromHome.clickWorkFromHome();
        Thread.sleep(1000);
        workFromHome.clickMyRequest();
        Thread.sleep(1000);
        workFromHome.clickOnDeleteButton();
        String Text5=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text5, "Work from home request was deleted");

    }

}
