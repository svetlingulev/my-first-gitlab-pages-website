package ui.tests;

import ui.pages.*;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.ConfigurationSystemPage;
import ui.pages.CustomUserFieldsPage;
import ui.pages.ReportPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ReportTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addReport() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        report.clickOnReport();
        Thread.sleep(1000);
        report.clickOnTemplates();
        Thread.sleep(1000);
        report.clickOnAddNew();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD TEMPLATE");
        report.typeName("1test");
        report.typeDD();
        report.addTeamsDD();
        report.addUserProperties();
        report.addProjects();
        report.addFunctionalities();
        report.addDepartment();
        report.addCustomUserProperties();
        report.addClients();
        report.clickSave();
        Thread.sleep(500);
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Template was saved");
    }

    @Test(priority = 2)
    public void editReport() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickOnTemplates();
        String Text = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(Text, "1test");
        Thread.sleep(1000);
        report.clickEdit();
        Thread.sleep(500);
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div")).getText();
        Assert.assertEquals(Text1, "EDIT TEMPLATE");
        report.typeEditedName();
        report.clickSave();
        Thread.sleep(500);
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Template was saved");
    }

    @Test(priority = 3)
    public void deleteReport() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        CustomUserFieldsPage userProfile = new CustomUserFieldsPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickOnTemplates();
        String Text = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(Text, "1edited name");
        Thread.sleep(500);
        userProfile.clickOnDelete();
        Thread.sleep(1000);
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Template was deleted");
    }

    @Test(priority = 4)
    public void createReport() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickCreateReport();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[1]")).getText();
        Assert.assertEquals(Text, "Report");
        Thread.sleep(500);
        report.startDate();
        report.endDate();
        report.periodDD();
        report.templatesDD();
        Thread.sleep(1000);
        report.clickGenerate();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[3]/form/div[2]/h2")).getText();
        Assert.assertEquals(Text1, "Report");
    }

    @Test(priority = 5)
    public void createQuery() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickQuery();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[1]/div")).getText();
        Assert.assertEquals(Text, "Query");
        report.typeName("test");
        report.queryTypeDD();
        report.startDate();
        report.queryTeams();
        report.queryDepartments();
        report.queryUserProperties();
        report.queryCustomProperties();
        report.queryProjects();
        report.queryClients();
        report.queryFunctionality();
        report.endDate();
        report.periodDD();
        report.clickQueryGenerate();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[3]/form/div[3]/h2")).getText();
        Assert.assertEquals(Text1, "Query");
    }

    @Test(priority = 6)
    public void psTimeSheet() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickPsTimesheet();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div[1]/div")).getText();
        Assert.assertEquals(Text, "Professional Service Timesheet");
        report.typeReportName();
        report.toDate();
        report.fromDate();
        report.teamDD();
        report.clickOnSheetsReport();
        Thread.sleep(1000);
        report.sheetedBy();
        report.clickTimesheetGenerate();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div[2]/div[1]/ul/li/a")).getText();
        Assert.assertEquals(Text1, "V_Murat(vedi)");

    }

    @Test(priority = 7)
    public void form76() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        report.clickOnReport();
        Thread.sleep(1000);
        report.clickForm76();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div[1]/div[1]")).getText();
        Assert.assertEquals(Text, "Form 76");
        report.month();
        report.companiesDD();
        report.clickForm76Generate();
        String Text1 = driver.findElement(By.xpath(".//*[@id='tabs']/table/thead/tr[1]/th[4]")).getText();
        Assert.assertEquals(Text1, "ДНИ ОТ МЕСЕЦА");

    }

    @Test(priority = 8)
    public void psMonthReport() throws InterruptedException {

        ReportPage report = new ReportPage(driver);
        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
//        Thread.sleep(1000);
//        system.clickConfiguration();
//        Thread.sleep(1000);
//        report.clickOnReport();
        Thread.sleep(1000);
        report.clickPsMonthReport();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div[1]/div")).getText();
        Assert.assertEquals(Text, "Monthly Professional Service Timesheet");
        report.typeReportName();
        Thread.sleep(1000);
        report.toOldDate();
        Thread.sleep(1000);
        report.fromOldDate();
        Thread.sleep(1000);
        report.teamDD();
        report.usersDD();
        Thread.sleep(1000);
        report.clickTimesheetGenerate();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div[2]/ul/li[1]/a")).getText();
        Assert.assertEquals(Text1, "Inferno");

    }

}
