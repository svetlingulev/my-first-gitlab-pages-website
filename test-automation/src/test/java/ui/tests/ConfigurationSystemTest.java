package ui.tests;

import ui.pages.ConfigurationSystemPage;
import ui.pages.TasksPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ConfigurationSystemTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test
    public void addSystemConfiguration() throws InterruptedException {

        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        TasksPage tasks = new TasksPage(driver);
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        system.clickSystem();
        system.typeEmailHost();
        system.typeEmailPort();
        system.typeEmailFrom();
        system.typeSMTPUsername();
        system.typeSMTPPassword();
        system.typeAdminPassword();
        system.typeAdminEmail();
        system.typeErrorAdminEmail();
        system.typeEmailDaysOff();
        Thread.sleep(1000);
        system.clickLDAP();
        Thread.sleep(1000);
        system.typeLdapHostName();
        system.typeLdapIP();
        system.typeLdapPort();
        system.clickJira();
        Thread.sleep(1000);
        system.typeJiraPassword();
        system.clickDevStats();
        Thread.sleep(1000);
        system.typeDevStatsPassword();
        system.clickReminders();
        Thread.sleep(1000);
        system.typeEmailNewAccounts();
        system.remindersDD();
        Thread.sleep(1000);
        system.clickAdministration();
        system.typeTestEmail("fenek114@abv.bg");
        Thread.sleep(1000);
        system.clickSend();
        Thread.sleep(3000);
        String Text=driver.findElement(By.xpath(".//*[@id='success']")).getText();
        Assert.assertEquals(Text, "Successful email sending");
        Thread.sleep(1000);
        system.clickSave();
        driver.get(props.getProperty("mailUrl"));
        //LoginPage login = new LoginPage(driver);
       // login.clickOnMailLogin();
      //  login.typeEmail(props.getProperty("mail"));
      //  login.typeMailPassword(props.getProperty("emailpass"));
      //  login.clickLogIn();
        Thread.sleep(1000);
        tasks.clickDemoInbox();
        Thread.sleep(1000);
        system.clickOnTestMail();
        String Text8 = driver.findElement(By.xpath(".//*[@id='main']/div/div[2]/div/div[2]/div[1]/div/div[1]/h2")).getText();
        Assert.assertEquals(Text8, "Test email sender credentials");
    }

    @Test
    public void checkEvent() throws InterruptedException {

        ConfigurationSystemPage system = new ConfigurationSystemPage(driver);
        driver.get(props.getProperty("url"));
        Thread.sleep(1000);
        system.clickConfiguration();
        Thread.sleep(1000);
        system.clickOnEventLogs();
        Thread.sleep(1000);
        system.fromDate();
        Thread.sleep(500);
        system.toDate();
        Thread.sleep(1000);
        system.eventLogTypeDD();
        system.userDD();
        system.typeMessage("Sending email to fenek114@abv.bg with subject \"Test email sender credentials\"");
        system.clickSearch();
        Thread.sleep(1000);
        String Text8 = driver.findElement(By.xpath(".//*[@id='jobOffers-list']/tbody/tr[1]/td[4]")).getText();
        Assert.assertEquals(Text8, "Sending email to fenek114@abv.bg with subject \"Test email sender credentials\"");

    }


}
