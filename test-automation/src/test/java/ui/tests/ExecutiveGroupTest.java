package ui.tests;

import ui.pages.ExecutiveGroupPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ExecutiveGroupTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addExecutiveGroup() throws InterruptedException {

        ExecutiveGroupPage executiveGroup = new ExecutiveGroupPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        executiveGroup.clickExecutiveGroup();
        Thread.sleep(500);
        executiveGroup.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "ADD EXECUTIVE GROUP");
        executiveGroup.typeName("An test Executive Group");
        Thread.sleep(500);
        executiveGroup.addExecutiveGroup();
        Thread.sleep(500);
        executiveGroup.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Executive Group was saved");
    }

    @Test(priority = 2)
    public void editExecutiveGroup() throws InterruptedException {

        ExecutiveGroupPage executiveGroup = new ExecutiveGroupPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        executiveGroup.clickExecutiveGroup();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Executive Groups");
        Thread.sleep(500);
        executiveGroup.clickEdit();
        Thread.sleep(500);
        executiveGroup.editExecutiveGroup();
        Thread.sleep(500);
        executiveGroup.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Executive Group was saved");
    }

    @Test(priority = 3)
    public void deleteExecutiveGroup() throws InterruptedException {

        ExecutiveGroupPage executiveGroup = new ExecutiveGroupPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        executiveGroup.clickExecutiveGroup();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Executive Groups");
        Thread.sleep(500);
        executiveGroup.clickOnDeleteButton();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Executive Group was deleted");
    }

}


