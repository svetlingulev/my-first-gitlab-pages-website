package ui.tests;

import ui.pages.JobOffersPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class JobOffersTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        Thread.sleep(1000);
        job.clickAddNewButton();
        job.typeTitle("testjob");
        Thread.sleep(1000);
        job.addTargetCompanies();
        job.typeDescription("testjob");
        job.typeContent("testjob");
        job.clickIsSticky();
        job.clickIsActive();
        job.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Job Offer was saved");
    }

    @Test(priority = 2)
    public void listJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        job.fromDate();
        job.typeTitle2("testjob");
        job.typeShortDescription("testjob");
        job.selectStatus();
        Thread.sleep(1000);
        job.clickSearch();
        Thread.sleep(1000);
        String Text=driver.findElement(By.xpath("(//td[@class='text-left col-md-2'])[1]")).getText();
        Assert.assertEquals(Text, "testjob");
    }

    @Test(priority = 3)
    public void editJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        job.typeTitle2("testjob");
        Thread.sleep(1000);
        job.clickSearch();
        Thread.sleep(1000);
        job.clickEdit();
        job.editTitle();
        Thread.sleep(1000);
        job.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Job Offer was saved");
    }

    @Test(priority = 6)
    public void deleteJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        job.typeTitle2("editedjob");
        Thread.sleep(1000);
        job.clickSearch();
        Thread.sleep(1000);
        job.clickDelete();
        Thread.sleep(1000);
        job.clickDeleteConfirm();
        String Text2=driver.findElement(By.xpath("//p[text()='Job Offer was deleted']")).getText();
        Assert.assertEquals(Text2, "Job Offer was deleted");
    }

    @Test(priority = 4)
    public void addInvalidJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        Thread.sleep(1000);
        job.clickAddNewButton();
        job.typeTitle("testjob");
        Thread.sleep(1000);
        job.typeDescription("testjob");
        job.clickSave();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/ul/li")).getText();
        Assert.assertEquals(Text, "Content required");
    }

    @Test(priority = 5)
    public void listnonexistingJobOffers() throws InterruptedException {

        JobOffersPage job = new JobOffersPage(driver);
        Thread.sleep(1000);
        job.clickJobOffers();
        job.typeTitle2("qqq");
        Thread.sleep(1000);
        job.clickSearch();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/form/div[2]/div")).getText();
        Assert.assertEquals(Text, "No Job Offers");
    }

}
