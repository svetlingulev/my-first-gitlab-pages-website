package ui.tests;

import ui.pages.HolidayPage;
import ui.pages.LocationUnitPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class LocationUnitTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
     //   login.typeUserName(props.getProperty("username"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addLocationUnit() throws InterruptedException {

        LocationUnitPage locationUnits = new LocationUnitPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        locationUnits.clickLocationUnits();
        Thread.sleep(1000);
        locationUnits.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD LOCATION UNIT");
        locationUnits.addCountry();
        locationUnits.addCity();
        locationUnits.addLocation();
        locationUnits.typeName("A test location unit");
        Thread.sleep(500);
        locationUnits.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Location Unit was saved");
    }

    @Test(priority = 2)
    public void editLocationUnit() throws InterruptedException {

        LocationUnitPage locationUnits = new LocationUnitPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[24]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        locationUnits.clickLocationUnits();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Location Units");
        Thread.sleep(500);
        locationUnits.clickEdit();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "EDIT LOCATION UNIT");
        locationUnits.editLocationUnit();
        Thread.sleep(500);
        locationUnits.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Location Unit was saved");
    }

    @Test(priority = 3)
    public void deleteLocationUnit() throws InterruptedException {

        LocationUnitPage locationUnits = new LocationUnitPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[23]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        locationUnits.clickLocationUnits();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Location Units");
        Thread.sleep(500);
        locationUnits.clickOnDeleteButton();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Location Unit was deleted");

    }

}


