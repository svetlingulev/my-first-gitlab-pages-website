package ui.tests;

import ui.pages.TimeOffPage;
import ui.pages.WorklogsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class TimeOffTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void requestTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDate();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDate();
        timeOff.clickSomewhere();
        timeOff.clickHalfDay();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time off request was saved");
    }

    @Test(priority = 2)
    public void requestReport() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequestReport();
        Thread.sleep(1000);
        timeOff.reportStartDay();
        Thread.sleep(1000);
        timeOff.addRequestReportDD();
        Thread.sleep(1000);
        timeOff.clickGenerate();
        String Text = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[1]")).getText();
        Assert.assertEquals(Text, "Admin Admin");
    }

    @Test(priority = 3)
    public void dublicateTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDate();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDate();
        timeOff.clickSomewhere();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
//        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/ul/li")).getText();
//        Assert.assertEquals(Text2, "There are more than allowed Time Off Requests or logged work hours for 2016-12-22 that doesn't allow you to make this request.");
    }

    @Test(priority = 4)
    public void weekendTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDateWeekend();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDateWeekend();
        Thread.sleep(1000);
        timeOff.clickSomewhere();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/ul/li")).getText();
        Assert.assertEquals(Text2, "You must have at least one workday in the selected period");
    }

    @Test(priority = 5)
    public void listTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickAllTineOff();
        Thread.sleep(1000);
        timeOff.selectTimeOffgroup();
        timeOff.selectUser();
        Thread.sleep(1000);
        timeOff.clickSearchBtn();
        String Text2=driver.findElement(By.xpath(".//*[@id='pending-time-offs']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text2, "Admin Admin");
    }

    @Test(priority = 6)
    public void deleteTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickMyRequests();
        Thread.sleep(1000);
        String Text1 = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text1, "Special time off");
        timeOff.clickOnDeleteButton();
        Thread.sleep(1000);
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time off request was deleted");
    }

    @Test(priority = 7)
    public void approveTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDate();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDate();
        timeOff.clickSomewhere();
        timeOff.clickHalfDay();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time off request was saved");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
     //   LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("userAdmin"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickMnaganeRequests();
        Thread.sleep(1000);
        String Text3 = driver.findElement(By.xpath(".//*[@id='pending ptos']/tbody/tr/td[6]")).getText();
        Assert.assertEquals(Text3, "Special time off");
        Thread.sleep(1000);
        timeOff.clickApprove();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Time off request was approved");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
    //    login.clickOnLoginButton();
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickApprovedRequests();
        timeOff.selectTimeOffgroup2();
        Thread.sleep(1000);
        timeOff.selectUser2();
        Thread.sleep(1000);
        timeOff.clickSearch2();
        Thread.sleep(1000);
        String Text5 = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr[1]/td[4]")).getText();
        Assert.assertEquals(Text5, "Special time off");
        timeOff.clickMyRequests();
        Thread.sleep(1000);
        timeOff.clickDelete2();
        Thread.sleep(1000);
        timeOff.clickdeleteComfirm();
        Thread.sleep(1000);
        String Text6 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text6, "Time off request deletion send");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
     //   login.typeUserName(props.getProperty("userAdmin"));
     //   login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        timeOff.clickMnaganeRequests();
        Thread.sleep(1000);
        timeOff.clickApproveDelete();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Time off request was deleted");
    }


    @Test(priority = 8)
    public void rejectTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
        WorklogsPage worklog = new WorklogsPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDate();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDate();
        timeOff.clickSomewhere();
        timeOff.clickHalfDay();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time off request was saved");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("userAdmin"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        timeOff.clickMnaganeRequests();
        Thread.sleep(1000);
        String Text3 = driver.findElement(By.xpath(".//*[@id='pending ptos']/tbody/tr/td[6]")).getText();
        Assert.assertEquals(Text3, "Special time off");
        Thread.sleep(1000);
        timeOff.clickReject();
        String Text4 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text4, "Time off request was rejected");
        Thread.sleep(1000);
        worklog.clickOnAdminButton();
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        Thread.sleep(1000);
        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickMyRequests();
        String Text5 = driver.findElement(By.xpath(".//*[@id='dataTables-example']/tbody/tr/td[7]")).getText();
        Assert.assertEquals(Text5, "Rejected");
        Thread.sleep(1000);
        timeOff.clickDelete2();
        Thread.sleep(1000);
        timeOff.clickdeleteComfirm();
        String Text7 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text7, "Time off request was deleted");
    }

    @Test(priority = 9)
    public void commentTimeOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickRequest();
        Thread.sleep(1000);
        timeOff.addTimeOffgroup();
        timeOff.startDate();
        timeOff.clickSomewhere();
        Thread.sleep(1000);
        timeOff.endDate();
        timeOff.clickSomewhere();
        timeOff.clickHalfDay();
        timeOff.clickSubmit();
        Thread.sleep(1000);
        timeOff.clickConfirm();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time off request was saved");
        Thread.sleep(1000);
        timeOff.clickComment();
        Thread.sleep(1000);
        timeOff.typeComment("test");
        timeOff.clickSubmitComment();
        Thread.sleep(1000);
        timeOff.clickBack();
        Thread.sleep(1000);
        timeOff.clickDelete2();
        Thread.sleep(1000);
        timeOff.clickdeleteComfirm();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Time off request was deleted");

    }

    @Test(priority = 10)
    public void addTimeOffgroup() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickGroups();
        Thread.sleep(1000);
        timeOff.addNewgroup();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD NEW TIME OFF GROUP");
        timeOff.typeGroupName("test");
        timeOff.typeAnnualAllowance();
        timeOff.addApprover();
        timeOff.typeDescription("test");
        timeOff.clickNotify();
        timeOff.clickSaveGroup();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time Off Group was saved");
    }

    @Test(priority = 11)
    public void editTimeOffgroup() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickGroups();
        Thread.sleep(1000);
        timeOff.clickEditGroup();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "EDIT TIME OFF GROUP");
        Thread.sleep(1000);
        timeOff.editDescription();
        Thread.sleep(1000);
        timeOff.clickSaveGroup();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Time Off Group was saved");
        Thread.sleep(1000);
        String Text3 = driver.findElement(By.xpath(".//*[@id='TimeOffGroups']/tbody/tr[5]/td[8]")).getText();
        Assert.assertEquals(Text3, "edited test");
    }

    @Test(priority = 12)
    public void deleteTimeOffgroup() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickGroups();
        Thread.sleep(1000);
        timeOff.clickDeleteGroup();
        Thread.sleep(1000);
        timeOff.clickdeleteComfirm();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Time Off Group was deleted");
    }

    @Test(priority = 12)
    public void userDaysOff() throws InterruptedException {

        TimeOffPage timeOff = new TimeOffPage(driver);
//        Thread.sleep(1000);
//        timeOff.clickTimeOff();
        Thread.sleep(1000);
        timeOff.clickUsersDaysOff();
        Thread.sleep(1000);
        timeOff.userDD();
        timeOff.companyDD();
        timeOff.timeOffGroupDD();
        timeOff.yearDD();
        timeOff.userStatusDD();
        timeOff.clickSearch();
        String Text = driver.findElement(By.xpath(".//*[@id='TimeOffGroups']/tbody/tr/td[1]")).getText();
        Assert.assertEquals(Text, "Admin Admin");
    }
}
