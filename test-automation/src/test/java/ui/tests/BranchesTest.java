package ui.tests;

import ui.pages.BranchesPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class BranchesTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addBranches() throws InterruptedException {

        BranchesPage teams = new BranchesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        teams.clickBranches();
        Thread.sleep(500);
        teams.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "ADD NEW BRANCH");
        teams.typeName("A test branch");
        Thread.sleep(500);
        teams.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Branch was saved");
    }

    @Test(priority = 2)
    public void editBranches() throws InterruptedException {

        BranchesPage teams = new BranchesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        teams.clickBranches();
        Thread.sleep(500);
        teams.clickEdit();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "EDIT BRANCH");
        teams.editName();
        Thread.sleep(500);
        teams.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Branch was saved");
    }

    @Test(priority = 3)
    public void deleteBranches() throws InterruptedException {

        BranchesPage teams = new BranchesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        teams.clickBranches();
        Thread.sleep(500);
        teams.clickOnDeleteButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Branch was deleted");
    }

}


