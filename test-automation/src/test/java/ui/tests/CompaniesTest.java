package ui.tests;

import ui.pages.CompaniesPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CompaniesTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
     //   LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addComapnies() throws InterruptedException {

        CompaniesPage companies = new CompaniesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        companies.clickCompanies();
        Thread.sleep(500);
        companies.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "ADD COMPANY");
        companies.parentCompanyDD();
        companies.typeName("1st company test");
        companies.typeCity("test");
        companies.typeAddress("test street");
        companies.typeOwner("test");
        companies.typeEIK("1234567");
        Thread.sleep(500);
        companies.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Company was saved");
    }

    @Test(priority = 2)
    public void editCompanies() throws InterruptedException {

        CompaniesPage companies = new CompaniesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        companies.clickCompanies();
        Thread.sleep(500);
        companies.clickEdit();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "EDIT COMPANY");
        companies.editName();
        Thread.sleep(500);
        companies.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Company was saved");
    }

    @Test(priority = 3)
    public void deleteCompanies() throws InterruptedException {

        CompaniesPage companies = new CompaniesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        companies.clickCompanies();
        Thread.sleep(500);
        companies.clickOnDeleteButton();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text, "Company was deleted");
    }

}


