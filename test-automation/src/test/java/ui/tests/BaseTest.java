package ui.tests;

import org.openqa.selenium.WebDriver;
import ui.properties.ConfigProperties;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class BaseTest {
    //public WebDriverFactory driver;
    public WebDriverWait wait;
    public DriverManager driverManager;
    public WebDriver driver;


    private String getParameter(String name) {
        ConfigProperties cfg = new ConfigProperties();
        String value = System.getProperty(name);
        if (value == null)
            value = cfg.defaultBrowser;

        if (value.isEmpty())
            value = cfg.defaultBrowser;


        return value;
    }

    @BeforeClass
    public void setup () {

        String browserName = getParameter("browser");
        driverManager = DriverManagerFactory.getManager(browserName);
        driver = driverManager.getDriver();


        //Create a wait. All test and page classes use this wait.
        wait = new WebDriverWait(driver,35);

        //Maximize Window
        driver.manage().window().maximize();
    }

    @AfterClass
    public void teardown () {
        driver.quit();
    }
}