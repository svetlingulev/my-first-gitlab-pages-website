package ui.tests;

import ui.pages.HolidayPage;
import ui.pages.OccupationalClassificationsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class OccupationalClassificationTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
      //  login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addOccupationalClassification() throws InterruptedException {

        OccupationalClassificationsPage classification = new OccupationalClassificationsPage(driver);
        HolidayPage holiday = new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        classification.clickOccupationalClassification();
        Thread.sleep(500);
        classification.clickAddNew();
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div/div/div/span")).getText();
        Assert.assertEquals(Text, "ADD OCCUPATIONAL CLASSIFICATION");
        classification.typeName("A test classification");
        classification.typeCode("1234567");
        classification.typeDescription("test classification");
        classification.typeSocialSecurity();
        Thread.sleep(500);
        classification.clickSave();
        String Text1 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Occupational Classification was saved");
    }

    @Test(priority = 2)
    public void listOccupationalClassification() throws InterruptedException {

        OccupationalClassificationsPage classification = new OccupationalClassificationsPage(driver);
        HolidayPage holiday = new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        classification.clickOccupationalClassification();
        Thread.sleep(500);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Occupational Classifications");
        classification.typeName("A test classification");
        classification.typeCode("1234567");
        classification.typeDescription("test classification");
        classification.typeSocialSecurity();
        Thread.sleep(500);
        classification.clickSearch();
        String Text1 = driver.findElement(By.xpath("//td[text()='A test classification']")).getText();
        Assert.assertEquals(Text1, "A test classification");
    }

    @Test(priority = 3)
    public void editOccupationalClassification() throws InterruptedException {

        OccupationalClassificationsPage classification = new OccupationalClassificationsPage(driver);
        HolidayPage holiday = new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        classification.clickOccupationalClassification();
        Thread.sleep(500);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Occupational Classifications");
        classification.typeName("A test classification");
        Thread.sleep(500);
        classification.clickSearch();
        String Text1 = driver.findElement(By.xpath("//td[text()='A test classification']")).getText();
        Assert.assertEquals(Text1, "A test classification");
        Thread.sleep(500);
        classification.clickEdit();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text2, "EDIT OCCUPATIONAL CLASSIFICATION");
        classification.editName();
        Thread.sleep(500);
        classification.clickSave();
        String Text3 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text3, "Occupational Classification was saved");
    }

    @Test(priority = 4)
    public void deleteOccupationalClassification() throws InterruptedException {

        OccupationalClassificationsPage classification = new OccupationalClassificationsPage(driver);
        HolidayPage holiday = new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        classification.clickOccupationalClassification();
        Thread.sleep(500);
        String Text = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div/div[1]")).getText();
        Assert.assertEquals(Text, "Occupational Classifications");
        classification.typeName("Edited test classification");
        Thread.sleep(500);
        classification.clickSearch();
        String Text1 = driver.findElement(By.xpath("//td[text()='Edited test classification']")).getText();
        Assert.assertEquals(Text1, "Edited test classification");
        Thread.sleep(500);
        classification.clickOnDeleteButton();
        String Text2 = driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Occupational Classification was deleted");
    }
}


