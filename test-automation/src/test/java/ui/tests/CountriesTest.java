package ui.tests;

import ui.pages.CountriesPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class CountriesTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
      //  LoginPage login = new LoginPage(driver);
      //  login.typeUserName(props.getProperty("username"));
      //  login.typePassword(props.getProperty("password"));
     //   login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addCountry() throws InterruptedException {

        CountriesPage countries = new CountriesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        countries.clickCountries();
        Thread.sleep(1000);
        countries.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD COUNTRY");
        countries.typeName("A test country");
        Thread.sleep(500);
        countries.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Country was saved");
    }

    @Test(priority = 2)
    public void editCountry() throws InterruptedException {

        CountriesPage countries = new CountriesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[22]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        countries.clickCountries();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Countries");
        Thread.sleep(500);
        countries.clickEdit();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "EDIT COUNTRY");
        countries.editName();
        Thread.sleep(500);
        countries.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Country was saved");
    }

    @Test(priority = 3)
    public void deleteCountry() throws InterruptedException {

        CountriesPage countries = new CountriesPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(".//*[@id='administration']/li[22]/a/span")));
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        countries.clickCountries();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Countries");
        Thread.sleep(500);
        countries.clickOnDeleteButton();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Country was deleted");
    }

}


