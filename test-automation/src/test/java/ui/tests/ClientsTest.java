package ui.tests;

import ui.pages.ClientsPage;
import ui.pages.HolidayPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ClientsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");

    @BeforeClass
    public void beforeClass() throws IOException {

        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(props.getProperty("url"));
       // LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addClients() throws InterruptedException {

        ClientsPage clients = new ClientsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        clients.clickClients();
        Thread.sleep(1000);
        clients.clickAddNew();
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text, "ADD CLIENT");
        clients.typeClientsName("A test client");
        Thread.sleep(500);
        clients.clickSave();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text1, "Client was saved");
    }

    @Test(priority = 2)
    public void editClients() throws InterruptedException {

        ClientsPage clients = new ClientsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        clients.clickClients();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Clients");
        clients.clickEdit();
        String Text1=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/div/div/div[1]/div/span")).getText();
        Assert.assertEquals(Text1, "EDIT CLIENT");
        clients.editedClientsName();
        Thread.sleep(500);
        clients.clickSave();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Client was saved");
    }

    @Test(priority = 3)
    public void deletetClients() throws InterruptedException {

        ClientsPage clients = new ClientsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        clients.clickClients();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Clients");
        clients.clickOnDeleteButton();
        String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Client was deleted");
    }

}


