package ui.tests;

import ui.pages.HolidayPage;
import ui.properties.ConfigProperties;
import ui.pages.ProjectsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Created by ddzhonova on 16.11.16.
 */
public class ProjectsTest {

    WebDriver driver;
    Properties props = new Properties();
    File file = new File("src/main/resources/config.properties");
    File log4jConfig = new File("src/main/resources/log4j.properties");

    static Logger logger = Logger.getLogger(ProjectsTest.class);
    //initializing the config class
    ConfigProperties cfg = new ConfigProperties();

    @BeforeClass
    public void beforeClass() throws IOException {


        //PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure(new FileInputStream(log4jConfig));
        props.load(new FileInputStream(file));
//        System.setProperty("webdriver.chrome.driver", "/home/ddzhonova/chromedriver");
        ChromeDriverManager.getInstance().setup();
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(props.getProperty("url"));
        //logger.debug("======================================================================");
        //getting the config value, by key
        //logger.debug(cfg.ReadProperty("username"));

      //  LoginPage login = new LoginPage(driver);
       // login.typeUserName(props.getProperty("username"));
       // login.typePassword(props.getProperty("password"));
       // login.clickOnLoginButton();
        assert driver.getTitle().startsWith("Home - Inferno");
    }

//    @AfterClass
//    public void afterClass() {
//        driver.quit();
//    }

    @Test(priority = 1)
    public void addProjects() throws InterruptedException {

        //Log in console in and log file
        logger.debug("Log4j appender configuration is successful !!");
        logger.debug(cfg.username);

        ProjectsPage projects = new ProjectsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
        Thread.sleep(500);
        holiday.clickConfiguration();
        Thread.sleep(500);
        projects.clickProjects();
        Thread.sleep(1000);
        projects.clickAddNew();
        Thread.sleep(1000);
        Assert.assertFalse( By.id("myModalLabel").toString().contains("Add"));
        projects.modalNewProject("Test new project");

        String Text1=driver.findElement(By.xpath("//*[@class='caption-subject font-blue-sharp bold uppercase']")).getText();
        Assert.assertEquals(Text1, "ADD PROJECT");



        projects.typeProjectsName("");
        projects.typeJiraProjectsName("test");
        projects.clientsDD();
        projects.branchDD();
        projects.TypeDD();

        projects.directorOfProductionDD();
        projects.productionManagerDD();
        projects.projectManagerDD();
        projects.typeBusinessValue("some business value");

       // projects.assignedUserDD();
        //projects.isBillable();
        Thread.sleep(500);
        projects.clickSave();
    }

    @Test(priority = 2)
    public void editProjects() throws InterruptedException {
        Thread.sleep(2000);
        ProjectsPage projects = new ProjectsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        projects.clickProjects();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Projects");

        projects.sortProjectsByName();
        Thread.sleep(2000);
        projects.sortProjectsByName();
        Thread.sleep(2000);

        projects.clickEdit();
        Thread.sleep(1000);
        projects.clickEditMenu();


        boolean exitstLinkbutton = driver.findElements( By.id("link-button") ).size() != 0;
        Assert.assertTrue(exitstLinkbutton);


        projects.editProjectsName();
        Thread.sleep(500);
        projects.clickSave();
        Thread.sleep(500);
        boolean alertSuccess = driver.findElements( By.className("alert-success")).size() != 0;
        Assert.assertTrue(alertSuccess);

    }

    @Test(priority = 3)
    public void deletetProjects() throws InterruptedException {
        Thread.sleep(2000);
        ProjectsPage projects = new ProjectsPage(driver);
        HolidayPage holiday= new HolidayPage(driver);
//        Thread.sleep(500);
//        holiday.clickConfiguration();
        Thread.sleep(500);
        projects.clickProjects();
        Thread.sleep(500);
        String Text=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/div[1]/div[1]/span")).getText();
        Assert.assertEquals(Text, "Projects");
        projects.clickOnDeleteButton();
        Thread.sleep(1000);
        projects.clickDeleteMenu();
        Thread.sleep(1000);
        projects.cofirmDelete();

        /*String Text2=driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[1]/p")).getText();
        Assert.assertEquals(Text2, "Project was deleted");*/
    }

}


